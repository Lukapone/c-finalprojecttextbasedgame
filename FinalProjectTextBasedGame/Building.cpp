
#include"Building.h"
#include"PrintUtility.h"
#include"StringUtility.h"
#include<iostream>
#include"BufferManipulation.h"
using std::string;		using std::cout;
using std::endl;

Building::Building() : Building(DEFAULT_BUILDING_NAME, DEFAULT_DESCRIPTION,DEFAULT_X_LOCATION,DEFAULT_Y_LOCATION){}

string Building::getBuildingName() const
{
	return m_name;
}

string Building::getBuildingDescription() const
{
	return m_description;
}

int Building::getBuildingXPos() const
{
	return m_xPos;
}

int Building::getBuildingYPos() const
{
	return m_yPos;
}

//functions which checks if there is key in the building and changes the pointer passed by reference to the key we want and returns true if sucesfull
//order is O(c) 
bool Building::getKeyPtr(Object*& p_keyToGet)
{
	if (m_keyPtr == nullptr)
	{
		cout << "There is no key in the building." << endl;
	}
	else
	{
		cout << "You got the key from building." << endl;
		m_keyPtr->setObjXPos(-1);
		p_keyToGet = m_keyPtr;
		m_keyPtr = nullptr;
		return true;
	}
	return false;
}

void Building::setKeyPtr(Object* p_key)
{
	m_keyPtr = p_key;
}

void Building::print() const
{
	printlnCenter("--------------------------------------------------");
	printlnCenter("Building name: " + m_name);
	printlnCenter("--------------------------------------------------");
	cout << "Building descr: " << endl;
	cout << m_description << endl;
	//cout << "Building X POS: " << m_xPos << " Building Y POS: " << m_yPos << endl;
}


//functions which saves the building in binary file
void Building::saveBuildingInBinary(File_handle& p_fileToSave)const
{
	p_fileToSave.writeCharBuffer(m_name);
	p_fileToSave.writeCharBuffer(m_description);
	p_fileToSave.writeInt(m_xPos);
	p_fileToSave.writeInt(m_yPos);

}

//function which loads the building from buffer according the parameters passed
void Building::loadBuildingFromBuffer(int p_nameSize, int p_descSize, int& p_offset, char* p_buffer)
{
	m_name = getStringFromBuffer(p_buffer, p_offset, p_nameSize);
	m_description = getStringFromBuffer(p_buffer, p_offset, p_descSize);
	m_xPos = getIntfromBuffer(p_buffer, p_offset);
	m_yPos = getIntfromBuffer(p_buffer, p_offset);
}











#ifndef BUILDING_H
#define BUILDING_H
//class which models a building in game

#include<string>
#include<utility>
#include"File_handle.h"
#include"Object.h"

static const char DEFAULT_BUILDING_NAME[] { "noBuildingName" };
static const char DEFAULT_DESCRIPTION[] {"noDescription" };
static const int DEFAULT_X_LOCATION = 0;
static const int DEFAULT_Y_LOCATION = 0;

class Building
{
private:
	std::string m_name;
	std::string m_description;
	int m_xPos;
	int m_yPos;
	Object* m_keyPtr;
	
public:
	Building();

	//full constructor
	template<class STR1,class STR2,class INT>
	Building(STR1&& p_name,STR2&& p_description,INT&&p_xPos,INT&&p_yPos):
		m_name(std::forward<STR1>(p_name)),
		m_description(std::forward<STR2>(p_description)),
		m_xPos(std::forward<INT>(p_xPos)),
		m_yPos(std::forward<INT>(p_yPos))
	{}

	//getters and setters
	std::string getBuildingName() const;
	std::string getBuildingDescription() const;
	int getBuildingXPos() const;
	int getBuildingYPos() const;
	bool getKeyPtr(Object*& p_keyToGet);

	void setKeyPtr(Object* p_key);

	template<class T>
	void setBuildingXPos(T&& p_xPos)
	{
		m_xPos = std::forward<T>(p_xPos);
	}

	template<class T>
	void setBuildingYPos(T&& p_yPos)
	{
		m_yPos = std::forward<T>(p_yPos);
	}

	template<class T>
	void setBuildingName(T&& p_name)
	{
		m_name = std::forward<T>(p_name);
	}

	template<class T>
	void setBuildingDescription(T&& p_desc)
	{
		m_description = std::forward<T>(p_desc);
	}

	//added functions
	void print() const;
	void saveBuildingInBinary(File_handle& p_fileToSave)const;

	void loadBuildingFromBuffer(int p_nameSize, int p_descSize, int& p_offset, char* p_buffer);

};





#endif
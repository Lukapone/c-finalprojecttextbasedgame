#ifndef PARSER_H
#define PARSER_H
#include<vector>
#include<string>
#include"Map.h"
#include"Player.h"
#include"LoadedBuildings.h"
#include"LoadedObjects.h"
#include"LoadedEnemies.h"
#include"LoginInfo.h"

//functions for parsing players input


bool parse(const std::vector<std::string>& sentence, Map& p_myMap, Player& p_player,LoadedBuildings& p_allBuildings,LoadedObjects& p_allObjs,LoadedEnemies& p_allEnemies,LoginInfo* p_loggedPlayer);





bool foundInSentence(const std::vector<std::string>& sentence, const std::string& toSearchFor);

void checkDirection(const std::vector<std::string>& sentence, Map& p_myMap, Player& p_player);





#endif
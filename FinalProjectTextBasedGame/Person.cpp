
#include<iostream>
#include"Person.h"
#include<vector>
#include<string>
#include"BufferManipulation.h"
#include"MathUtility.h"

using std::cout;		using std::endl;
using std::string;		using std::vector;

Person::Person() :Person(DEFAULT_NAME, DEFAULT_STR, DEFAULT_AGI, DEFAULT_CUR_HEALTH, DEFAULT_MAX_HEALTH, DEFAULT_LEVEL, DEFAULT_POS_PTR, DEFAULT_X_POS, DEFAULT_Y_POS){}


Person::~Person(){}


void Person::print() const
{
	//std::cout << "NAME: " << m_name
	//	<< " STR: " << m_strenght
	//	<< " AGI: " << m_agility
	//	<< " CUR_HEALTH: " << m_currentHealth
	//	<< " MAX_HEALTH: " << m_maxHealth
	//	<< " LEVEL: " << m_level << endl;
		//<< " POSITION: " << endl;
	//this->printMapFieldYouStandOn();
	//cout << " Pointer to pos: " << m_positionPtr << endl;
	cout << "	Name: " << m_name <<"	Level: "<<m_level<< "		Stats: " << m_strenght << " STR "  << m_agility << " AGI "  << m_currentHealth <<" FROM "<< m_maxHealth << " HEALTH" << endl;
	do_print();//calls pure virtual private function
}

void Person::setPositionPtr(MapField* p_mapFieldPtr)
{
	m_positionPtr = p_mapFieldPtr;
}

MapField Person::getPosition() const
{
	return *m_positionPtr;
}

MapField* Person::getPositionPtr() const
{
	return m_positionPtr;
}

//next 4 functions moves the person and checks for map boundries in direction he wants
//complexity is O(n) bigger the map longer it takes to check
bool Person::goNorth(Map& p_myMap)
{
	//cout << "Pointer of map field: " << m_position << endl;
	//m_position->printMapField();
	int movedYpos{ m_positionPtr->getY_coord() + 1 };
	//cout << "YPOS:" << movedYpos << endl;
	for (auto& mapField : p_myMap.getFieldsRef())
	{
		if (mapField.getX_coord() == m_positionPtr->getX_coord() && mapField.getY_coord() == movedYpos)
		{
			this->setPositionPtr(&mapField);
			return true;
		}
	}

	if (movedYpos != m_positionPtr->getY_coord()) //checks if we reached boundry
	{
		cout << "You are at the map boundry and canot go more north." << endl;
	}
	return false;
	//cout << "Pointer of map field after assigment: " << m_position << endl;
	//m_position->printMapField();
}

bool Person::goSouth(Map& p_myMap)
{
	//cout << "Pointer of map field: " << m_position << endl;
	//m_position->printMapField();
	int movedYpos{ m_positionPtr->getY_coord() - 1 };
	//cout << "YPOS:" << movedYpos << endl;
	for (auto& mapField : p_myMap.getFieldsRef())
	{
		if (mapField.getX_coord() == m_positionPtr->getX_coord() && mapField.getY_coord() == movedYpos)
		{
			this->setPositionPtr(&mapField);
			return true;
		}
	}

	if (movedYpos != m_positionPtr->getY_coord()) //checks if we reached boundry
	{
		cout << "You are at the map boundry and canot go more south." << endl;
	}
	return false;
	//cout << "Pointer of map field after assigment: " << m_position << endl;
	//m_position->printMapField();
}

bool Person::goEast(Map& p_myMap)
{
	//cout << "Pointer of map field: " << m_position << endl;
	//m_position->printMapField();
	int movedXpos{ m_positionPtr->getX_coord() + 1 };
	//cout << "YPOS:" << movedYpos << endl;
	for (auto& mapField : p_myMap.getFieldsRef())
	{
		if (mapField.getX_coord() == movedXpos && mapField.getY_coord() == m_positionPtr->getY_coord())
		{
			this->setPositionPtr(&mapField);
			return true;
		}
	}

	if (movedXpos != m_positionPtr->getX_coord()) //checks if we reached boundry
	{
		cout << "You are at the map boundry and canot go more east." << endl;
	}
	return false;
	//cout << "Pointer of map field after assigment: " << m_position << endl;
	//m_position->printMapField();
}

bool Person::goWest(Map& p_myMap)
{
	//cout << "Pointer of map field: " << m_position << endl;
	//m_position->printMapField();
	int movedXpos{ m_positionPtr->getX_coord() - 1 };
	//cout << "YPOS:" << movedYpos << endl;
	for (auto& mapField : p_myMap.getFieldsRef())
	{
		if (mapField.getX_coord() == movedXpos && mapField.getY_coord() == m_positionPtr->getY_coord())
		{
			this->setPositionPtr(&mapField);
			return true;
		}
	}

	if (movedXpos != m_positionPtr->getX_coord()) //checks if we reached boundry
	{
		cout << "You are at the map boundry and canot go more west." << endl;
	}
	return false;
	//cout << "Pointer of map field after assigment: " << m_position << endl;
	//m_position->printMapField();
}

//set the players position pointer from his x and y position
//complexity O(c)
void Person::setPositionPtrFromMap(Map& p_myMap)
{
	int offsetInVector{ 0 };
	offsetInVector = (m_xPos*p_myMap.getYlenght()) + m_yPos;//formula to get offset in vector for location
	m_positionPtr = &p_myMap.getFieldsRef()[offsetInVector];	
}

//function which adds to the inventory pointer of the object
//complexity O(c)
bool Person::pickUpPtr(Object* p_toPick)
{
	if (p_toPick == nullptr)
	{
		return false;//nothing there to pick up
	}
	else
	{
		m_inventory.addToInventoryPtr(p_toPick);
	}
	return true;
}

//function which will pick the object if it exists by that name in mapfield
//the worst complexity is O(n2)
bool Person::pickUpIfExists(const vector<string>& p_sentence )
{
	Object* toAdd = m_positionPtr->getObjPtrByNameAndRemove(p_sentence);//removes object from field and adds into inventory
	if (toAdd == nullptr)
	{
		return false;//nothing there to pick up
	}
	else
	{
		m_inventory.addToInventoryPtr(toAdd);
	}
	return true;
}

//function which will drop the object if it exists by that name into mapfield
bool Person::dropIfExists(const vector<string>& p_sentence)
{
	Object* toDrop = m_inventory.getObjPtrByNameAndRemove(p_sentence);//removes object from inventory and adds into field
	if (toDrop == nullptr)
	{
		return false;//nothing there to drop
	}
	else
	{
		m_positionPtr->addDroppedObjectPtr(toDrop);
	}
	return true;
}

//function which will equip the object if it exists by that name
void Person::equipIfExists(const std::vector<std::string>& p_sentence)
{
	 m_inventory.setToEquipped(p_sentence,this);
}

//function which will uequip the object if it exists by that name
void Person::unequipIfExists(const std::vector<std::string>& p_sentence)
{
	m_inventory.removeFromEquipped(p_sentence, this);
}

//function which will adds the bonuses of the object into players stats
//complexity O(c)
void Person::getObjectBonuses(Object* p_getBonuses)
{
	this->addStrenght(p_getBonuses->getObjStrAdd());
	this->addAgility(p_getBonuses->getObjAgiAdd());
	this->addMaxHealth(p_getBonuses->getObjmaxHealthAdd());
	this->healTotaly();
}

//function which will remove the bonuses of the object from players stats
//complexity O(c)
void Person::removeObjectBonuses(Object* p_getBonuses)
{
	this->addStrenght(-p_getBonuses->getObjStrAdd());
	this->addAgility(-p_getBonuses->getObjAgiAdd());
	this->addMaxHealth(-p_getBonuses->getObjmaxHealthAdd());
	this->healTotaly();
}
Inventory Person::getInventory() const
{
	return m_inventory;
}

void Person::printInventory()const
{
	cout << "INVENTORY:" << endl;
	m_inventory.print();
}

void Person::printMapFieldYouStandOn()const
{
	cout << "	YOU STAND ON:" << endl;
	if (m_positionPtr == nullptr)
	{
		cout << "Nullptr in players position " << endl;
	}
	else
	{
		m_positionPtr->print();
	}

}

std::string Person::getName() const
{
	return m_name;
}
int Person::getStrenght() const
{
	return m_strenght;
}
int Person::getAgility() const
{
	return m_agility;
}
int Person::getCurrentHealth() const
{
	return m_currentHealth;
}
int Person::getMaxHealth() const
{
	return m_maxHealth;
}
int Person::getLevel() const
{
	return m_level;
}
int Person::getXPos()const
{
	return m_xPos;
}
int Person::getYPos()const
{
	return m_yPos; 
}

//function which will damage the persons health and returns false if plyer is death
//complexity O(c)
bool Person::takeDamage(int p_damageToTake)
{
	m_currentHealth = m_currentHealth - p_damageToTake;
	if (m_currentHealth <= 0)//means you are death
	{
		return false;
	}
	return true;
}

//function which will set the persons location to be random within the map boundries
//complexity O(c)
void Person::respawnRandomLocation(Map& p_map)
{
	m_positionPtr = &p_map.getFieldsRef()[getRandCoord()];
}

//function which will heal person to full health
//complexity O(c)
void Person::healTotaly()
{
	m_currentHealth = m_maxHealth;
}

//function which will set the persons x and y pos from pointer of mapfield
//complexity O(c)
void Person::setXandYPosFromPtr()
{
	m_xPos = m_positionPtr->getX_coord();
	m_yPos = m_positionPtr->getY_coord();
}

//function which saves person to file and calls overloaded function to add save enemie or player
void Person::saveToFile(const string& p_fileName)const
{
	File_handle saveHandle(p_fileName, "wb");
	saveHandle.writeInt(m_name.size());//size for loading
	saveHandle.writeCharBuffer(m_name);
	saveHandle.writeInt(m_strenght);
	saveHandle.writeInt(m_agility);
	saveHandle.writeInt(m_currentHealth);
	saveHandle.writeInt(m_maxHealth);
	saveHandle.writeInt(m_level);
	saveHandle.writeInt(m_xPos);
	saveHandle.writeInt(m_yPos);
	this->do_saveBinary(saveHandle);
}

//function which loads person to file and calls overloaded function to add load enemie or player
void Person::loadFromFile(const string& p_fileName)
{
	File_handle loadHandle(p_fileName, "rb");
	int sizeOfName = loadHandle.readIntFromFileAtPointer();
	m_name=loadHandle.readStringFromFileAtPointer(sizeOfName);
	m_strenght = loadHandle.readIntFromFileAtPointer();
	m_agility = loadHandle.readIntFromFileAtPointer();
	m_currentHealth = loadHandle.readIntFromFileAtPointer();
	m_maxHealth = loadHandle.readIntFromFileAtPointer();
	m_level = loadHandle.readIntFromFileAtPointer();
	m_xPos = loadHandle.readIntFromFileAtPointer();
	m_yPos = loadHandle.readIntFromFileAtPointer();
	this->do_loadFromFile(loadHandle);
}


//function which saves person in binary
void Person::saveInBinary(File_handle& p_fileToSave)const
{
	p_fileToSave.writeCharBuffer(m_name);
	p_fileToSave.writeInt(m_strenght);
	p_fileToSave.writeInt(m_agility);
	p_fileToSave.writeInt(m_currentHealth);
	p_fileToSave.writeInt(m_maxHealth);
	p_fileToSave.writeInt(m_level);
	p_fileToSave.writeInt(m_xPos);
	p_fileToSave.writeInt(m_yPos);
	this->do_saveBinary(p_fileToSave);

}

//function which loads person from buffer
void Person::loadFromBuffer(int p_nameSize, int& p_offset, char* p_buffer)
{
	m_name = getStringFromBuffer(p_buffer, p_offset, p_nameSize);
	m_strenght = getIntfromBuffer(p_buffer, p_offset);
	m_agility = getIntfromBuffer(p_buffer, p_offset);
	m_currentHealth = getIntfromBuffer(p_buffer, p_offset);
	m_maxHealth = getIntfromBuffer(p_buffer, p_offset);
	m_level = getIntfromBuffer(p_buffer, p_offset);
	m_xPos = getIntfromBuffer(p_buffer, p_offset);
	m_yPos = getIntfromBuffer(p_buffer, p_offset);
	this->do_loadFromBuffer(p_buffer, p_offset);
}


void Person::addStrenght(int p_toAdd)
{
	m_strenght = m_strenght + p_toAdd;
}
void Person::addAgility(int p_toAdd)
{
	m_agility = m_agility + p_toAdd;
}
void Person::addCurrentHealth(int p_toAdd)
{
	m_currentHealth = m_currentHealth + p_toAdd;
}
void Person::addMaxHealth(int p_toAdd)
{
	m_maxHealth = m_maxHealth + p_toAdd;
}

//function which allow player to pick the key from building and checks if it exists and is guarded
//complaxity O(c)
bool Person::visitBuilding()
{
	if (m_positionPtr->hasEnemies() == true)
	{
		cout << "You have to defeat the enemies guarding the building" << endl;
	}
	else
	{
		if (m_positionPtr->hasBuilding() == true)
		{
			
			this->getKeyFromBuildings(m_positionPtr->getBuildingsPtrs());
			return true;
		}
		else
		{
			cout << "No building on the field to visit." << endl;
		}
	}
	return false;
}

//function which extracts key from buildings
void Person::getKeyFromBuildings(vector<Building*>& p_buildings)
{
	for (auto& building : p_buildings)
	{
		this->getKeyFromBuilding(building);
	}
}

//function which extracts key from building
void Person::getKeyFromBuilding(Building* p_building)
{
	Object* keyPtr{ nullptr };
	if (p_building->getKeyPtr(keyPtr))
	{
		m_inventory.addToInventoryPtr(keyPtr);
	}
}






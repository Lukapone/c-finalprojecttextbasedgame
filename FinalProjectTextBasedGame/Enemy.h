#ifndef ENEMY_H
#define ENEMY_H
//this is enemy class interface to the user
//enemy class that extends player class

#include"Person.h"

static const int DEFAULT_EXP_ADD = 0;

class Enemy : public Person //final helps compiler with calls of virtual functions
{
private:
	int m_experienceReward;

	virtual void do_print()const final override;
	virtual void do_saveBinary(File_handle& p_fileToSave)const final override;
	virtual void do_loadFromBuffer(char* p_buffer, int& p_offset)final override;
	virtual void do_loadFromFile(File_handle& p_fileToLoad)final override;
public:
	Enemy();

	template<typename STR, typename INT>
	Enemy(STR&& p_name, INT&& p_strenght, INT&& p_agility, INT&& p_currentHealth, INT&& p_maxHealth, INT&& p_level, INT&& p_experienceReward,MapField* p_positionPtr, INT&& p_xPos, INT&& p_yPos) :
		Person(std::forward<STR>(p_name),
		std::forward<INT>(p_strenght),
		std::forward<INT>(p_agility),
		std::forward<INT>(p_currentHealth),
		std::forward<INT>(p_maxHealth),
		std::forward<INT>(p_level),
		p_positionPtr,
		std::forward<INT>(p_xPos),
		std::forward<INT>(p_yPos)),
		m_experienceReward(std::forward<INT>(p_experienceReward))
	{}

	//getters and setters
	int getExperiencveReward()const;


};



#endif
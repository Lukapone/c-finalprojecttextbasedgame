
#include"LoginInfo.h"
#include<iostream>
#include"BufferManipulation.h"

using std::cout;		using std::endl;
using std::string;

LoginInfo::LoginInfo():m_userName(DEFAULD_UNAME),m_password(DEFAULD_PASSWORD),m_hasSave(false){};

void LoginInfo::print() const
{
	cout << "USERNAME: " << m_userName 
	<< "	PASSWORD: " << m_password << "	HAS_SAVE:"<<m_hasSave<<endl;
}

std::string LoginInfo::getName()const
{
	return m_userName;
}

std::string LoginInfo::getPassword()const
{
	return m_password;
}

bool LoginInfo::getHasSave()const
{
	return m_hasSave;
}


void LoginInfo::saveLoginInBinary(File_handle& p_fileToSave)const
{
	p_fileToSave.writeCharBuffer(m_userName);
	p_fileToSave.writeCharBuffer(m_password);
	p_fileToSave.writeChar(m_hasSave);
}

void LoginInfo::loadLoginFromBuffer(int p_nameSize,int p_passwordSize, int& p_offset, char* p_buffer)
{
	m_userName = getStringFromBuffer(p_buffer, p_offset, p_nameSize);
	m_password = getStringFromBuffer(p_buffer, p_offset, p_passwordSize);
	m_hasSave = getCharfromBuffer(p_buffer, p_offset);
}

#include <string>
#include <iostream>
#include <Windows.h>
#include"PrintUtility.h"
// implementation of print functionality
using std::string;		using std::cout;
using std::endl;		using std::cin;

//prints line and sets walue for delay for it to look like is beeing typed
//complexity is O(n) longer the string longer it takes
void println(const string str)
{
	for (unsigned i = 0; i < str.length(); i++)
	{
		//print each character
		cout << str[i];
		//you can adjust the time here
		//Sleep(20);  //for Visual C http://www.dreamincode.net/forums/topic/228382-make-text-to-appear-letter-by-letter-in-console/
		// http://www.cplusplus.com/forum/unices/60161/ 
	}
	cout << endl;
}
//prints centerred line with delay
//complexity is O(n) longer the string longer it takes
void printlnCenter(const string str)
{
	string centerd = centerString(str);
	for (unsigned i = 0; i < centerd.length(); i++)
	{
		cout << centerd[i];//be carefull when you are changing code
	}
	cout << endl;
}

//prints centerred line with delay
//complexity is O(n) longer the string longer it takes
void printlnCenterWithDelay(const string str)
{
	string centerd = centerString(str);
	for (unsigned i = 0; i < centerd.length(); i++)
	{
		cout << centerd[i];//be carefull when you are changing code
		//you can adjust the time here
		Sleep(20);
	}
	cout << endl;
}
//center string into middle of screen
//complexity is O(n) longer the string longer it takes
string centerString(const string toCenter)
{

	//Get the size and width of current window
	CONSOLE_SCREEN_BUFFER_INFO winInfo;

	// Handle is an object which is declare at the start.This is active console output screen buffer.
	HANDLE activeconsole = GetStdHandle(STD_OUTPUT_HANDLE);

	// this just initialize our window
	GetConsoleScreenBufferInfo(activeconsole, &winInfo);

	// this put width of current window into interger
	int colums;
	colums = winInfo.srWindow.Right - winInfo.srWindow.Left + 1;

	string centerSpace{ "" };
	unsigned l = toCenter.length();

	// this divide the current window int half
	unsigned pos = (int)((colums - l) / 2);

	for (unsigned i = 0; i < pos; i++)
	{
		centerSpace.push_back(' ');
	}

	return centerSpace + toCenter;

}
//center string into middle of screen and subtract value you desire from it
//complexity is O(n) longer the string longer it takes
string centerString(const string toCenter, int minus)
{

	//Get the size and width of current window
	CONSOLE_SCREEN_BUFFER_INFO winInfo;

	// Handle is an object which is declare at the start.This is active console output screen buffer.
	HANDLE activeconsole = GetStdHandle(STD_OUTPUT_HANDLE);

	// this just initialize our window
	GetConsoleScreenBufferInfo(activeconsole, &winInfo);

	// this put width of current window into interger
	int colums;
	colums = winInfo.srWindow.Right - winInfo.srWindow.Left + 1;

	string centerSpace = "";
	unsigned l = toCenter.length();

	// this divide the current window int half
	unsigned pos = (int)((colums - (l + minus)) / 2);

	for (unsigned i = 0; i < pos; i++)
	{
		centerSpace.push_back(' ');
	}

	return centerSpace + toCenter;

}

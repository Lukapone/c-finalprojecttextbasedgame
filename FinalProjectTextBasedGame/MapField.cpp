#include"MapField.h"
#include"PrintUtility.h"
#include<iostream>
#include"StringUtility.h"
#include"Enemy.h"

using std::cout;		using std::endl;
using std::vector;

MapField::MapField():MapField(DEFAULT_X_COORD, DEFAULT_Y_COORD)
{

}

vector<Building*>& MapField::getBuildingsPtrs()
{
	return m_buildingsOnFieldPtrs;
}

int MapField::getX_coord() const
{
	return m_xCoord;
}


int MapField::getY_coord() const
{
	return m_yCoord;
}

std::vector<Object*>& MapField::getObjectsPtrs()
{
	return m_objectsPtrs;
}

std::vector<Enemy*>& MapField::getEnemiesPtrs()
{
	return m_enemiesPtrs;
}

//prints all building on the maplfied
//complexity O(n)
void MapField::printAllBuildings()const
{
	cout << endl;
	printlnCenter("******************     BUILDINGS     ******************");
	cout << endl;
	if (m_buildingsOnFieldPtrs.empty())
		cout << "NONE" << endl;
	for (const auto& building : m_buildingsOnFieldPtrs)//prinst all buildings that field points to
	{
		if (building == nullptr)
		{
			cout << "nullptr in vector of buildings" << endl;//just for debug
		}
		else
		{
			building->print();
		}
	}
}

//prints all objects on the maplfied
//complexity O(n)
void MapField::printAllObjects()const
{
	cout << endl;
	printlnCenter("******************     OBJECTS     ******************");
	cout << endl;
	if (m_objectsPtrs.empty())
		cout << "NONE" << endl;
	for (const auto& object : m_objectsPtrs)//prints all objects that field points to 
	{
		if (object == nullptr)
		{
			cout << "nullptr in vector of objects" << endl;//just for debug
		}
		else
		{
			object->print();
		}
	}
}

//prints all enemies on the maplfied
//complexity O(n)
void MapField::printAllEnemies()const
{	
	cout << endl;
	printlnCenter("******************     ENEMIE(S)     ******************");
	cout << endl;
	if (m_enemiesPtrs.empty())
		cout << "NONE" << endl;
	for (const auto& enemy : m_enemiesPtrs)//prints all objects that field points to 
	{
		if (enemy == nullptr)
		{
			cout << "nullptr in vector of enemies" << endl;//just for debug
		}
		else
		{
			enemy->print();
		}
	}
}

//print the whole maplfield
void MapField::print() const
{
	cout << "		MapField X coord: " << m_xCoord << endl;
	cout << "		MapField Y coord: " << m_yCoord << endl;
	cout << "	YOU CAN SEE:" << endl;
	this->printAllBuildings();
	this->printAllObjects();
	this->printAllEnemies();
}

//function which adds object ptr into mapfielf and checks for nullptr
//complexity O(c)
void MapField::addObjectPtr(Object* p_objToAdd)
{
	if (p_objToAdd == nullptr)
	{
		cout << "You want to add nullptr in void MapField::addObjectPtr(Object* p_objToAdd)" << endl;
	}
	else
	{
		m_objectsPtrs.push_back(p_objToAdd);
	}
	
}

//function which adds object ptr into mapfielf and checks for nullptr plus changes its position
//complexity O(c)
void MapField::addDroppedObjectPtr(Object* p_objToAdd)
{
	if (p_objToAdd == nullptr)
	{
		cout << "You want to add nullptr in void MapField::addObjectPtr(Object* p_objToAdd)" << endl;
	}
	else
	{
		p_objToAdd->setObjXPos(m_xCoord);//set the position for save and reload to be sucesfull
		p_objToAdd->setObjYPos(m_yCoord);
		m_objectsPtrs.push_back(p_objToAdd);
	}

}

//function which adds building ptr into mapfielf and checks for nullptr
//complexity O(c)
void MapField::addBuildingPtr(Building* p_buildingToAdd)
{
	if (p_buildingToAdd == nullptr)
	{
		cout << "You want to add nullptr in void MapField::addBuildingPtr(Building* p_buildingToAdd)" << endl;
	}
	else
	{
		m_buildingsOnFieldPtrs.push_back(p_buildingToAdd);
	}
	
}

//function which adds enemie ptr into mapfielf and checks for nullptr
//complexity O(c)
void MapField::addEnemiePtr(Enemy* p_enemyToAdd)
{
	if (p_enemyToAdd == nullptr)
	{
		cout << "You want to add nullptr in void MapField::addEnemiePtr(Enemy* p_enemyToAdd)" << endl;
	}
	else
	{
		m_enemiesPtrs.push_back(p_enemyToAdd);
	}
}

//function which adds wounded enemie ptr into mapfielf and checks for nullptr and sets its position
//complexity O(c)
void MapField::addWoundetEnemiePtr(Enemy* p_enemyToAdd)
{
	if (p_enemyToAdd == nullptr)
	{
		cout << "You want to add nullptr in void MapField::addEnemiePtr(Enemy* p_enemyToAdd)" << endl;
	}
	else
	{
		p_enemyToAdd->setXPos(m_xCoord);
		m_enemiesPtrs.push_back(p_enemyToAdd);
	}
}

//function for picking up the object from mapfield
//complexity O(n2)
Object* MapField::getObjPtrByNameAndRemove(const std::vector<std::string>& p_sentence)
{
	Object* toReturn{ nullptr };
	for (const auto& word : p_sentence)
	{
		for (int i = 0; i != m_objectsPtrs.size(); i++)
		{
			if (toLowerCase(word) == toLowerCase(m_objectsPtrs[i]->getObjName()))
			{
				cout << "You picked up: "<<m_objectsPtrs[i]->getObjName() << endl;
				m_objectsPtrs[i]->setObjXPos(-1);
				toReturn = m_objectsPtrs[i];
				std::swap(m_objectsPtrs[i], m_objectsPtrs[m_objectsPtrs.size() - 1]);//swap elements for fast removal from vector just pop_back
				m_objectsPtrs.pop_back();
				return toReturn;
			}
		}
	}
	cout << "You didnt pick up anything."<< endl;
	return toReturn;
}

//function which checks for enemies on mapfield and starts fight with one
Enemy* MapField::getEnemyToFightAndRemoveIt()
{
	Enemy* toReturn{ nullptr };
	if (m_enemiesPtrs.empty())
	{
		cout << "There is nobody to fight." << endl;
	}
	else
	{
		cout << "You started fight with: " << m_enemiesPtrs[m_enemiesPtrs.size()-1]->getName() << endl;
		m_enemiesPtrs[m_enemiesPtrs.size() - 1]->setXPos(-1);
		toReturn = m_enemiesPtrs[m_enemiesPtrs.size() - 1];
		m_enemiesPtrs.pop_back();
	}	
	return toReturn;
}

//function to check if there are buildings on mapfield
//complexity O(c)
bool MapField::hasBuilding() const
{
	return !m_buildingsOnFieldPtrs.empty();
}

//function to check if there are enemies on mapfield
//complexity O(c)
bool MapField::hasEnemies() const
{
	return !m_enemiesPtrs.empty();
}








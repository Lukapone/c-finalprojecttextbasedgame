
#include"Map.h" //vector, utility

Map::Map() :Map(DEFAULT_X_LENGHT,DEFAULT_Y_LENGHT)
{

}

void Map::print() const
{
	for (const auto& mField : m_mapFields)
	{
		mField.print();
	}
}

//function which creates maplfields for map and puts them in
//complexity is O(n2)
void Map::populateMap()
{

	m_mapFields.reserve(m_xLenght*m_yLenght);//reserve the size i need
	for (int i = 0; i != m_xLenght; i++)
	{
		for (int b = 0; b != m_yLenght; b++)
		{
			m_mapFields.push_back(std::move(MapField(i, b)));
		}
	}

}


std::vector<MapField>& Map::getFieldsRef()
{
	return m_mapFields;
}

int Map::getXLenght() const
{
	return m_xLenght;
}

int Map::getYlenght() const
{
	return m_yLenght;
}


























#ifndef USERINPUTUTILITY_H
#define USERINPUTUTILITY_H
//navigational menu for the inputs from user

#include<string>
#include<vector>

//functions that takes user input
void expInput();
int userInputInt();
std::string userInputStr();
int userInputIntWithText(const std::string p_text);
std::string userInputStrWithText(const std::string p_text);
std::string userInputStrLineWithText(const std::string p_text);
std::vector<std::string> getAndSplitInput();
std::vector<std::string> split(const std::string& s);

#endif
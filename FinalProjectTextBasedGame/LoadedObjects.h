#ifndef LOADED_OBJECTS_H
#define LOADED_OBJECTS_H

#include"Object.h"
#include"Map.h"
#include"Player.h"
#include"LoadedBuildings.h"
//class which keeps track of all objects in game
class LoadedObjects
{
private:
	std::vector<Object> m_allObjcets; // use of vector because low memory consumption and fast random acces also the objects doesn't need to be in order

public:
	LoadedObjects();

	template<typename V>
	LoadedObjects(V&& p_allObjects) :
		m_allObjcets(std::forward<V>(p_allObjects)){}


	//getters and setters
	std::vector<Object>& getObjectsRef();

	template<typename T>
	void setObjects(T&& p_objectsToSet)
	{
		m_allObjcets = std::forward<T>(p_objectsToSet);
	}



	//added functionality
	void print() const;
	void populate();
	void putObjectsPtrsIntoMap(Map& p_myMap,Player& p_player,LoadedBuildings& p_allBuildings);
	
	std::vector<int> createMetaDataForFile() const;
	void saveAllObjectsIntoFile(const std::string& p_filename)const;
	void loadAllObjectsFromFile(const std::string& p_filename);

};




#endif
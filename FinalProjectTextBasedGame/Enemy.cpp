
#include"Enemy.h"
#include<iostream>
#include"BufferManipulation.h"
using std::cout;		using std::endl;

Enemy::Enemy() :Person(), m_experienceReward(DEFAULT_EXP_ADD){}

int Enemy::getExperiencveReward()const
{
	return m_experienceReward;
}

//overloaded print which adds to person print
void Enemy::do_print()const
{
	cout << "Experience add: " << m_experienceReward<< endl;
}

//overloaded function which adds to saving the enemy
void Enemy::do_saveBinary(File_handle& p_fileToSave)const
{
	p_fileToSave.writeInt(m_experienceReward);
}

//overloaded function which adds to loading the enemy from buffer
void Enemy::do_loadFromBuffer(char* p_buffer, int& p_offset)
{
	m_experienceReward = getIntfromBuffer(p_buffer, p_offset);
}

//overloaded function which adds to loading the enemy from file
void Enemy::do_loadFromFile(File_handle& p_fileToLoad)
{
	m_experienceReward = p_fileToLoad.readIntFromFileAtPointer();
}
#ifndef MAP_H
#define MAP_H

#include"MapField.h" //has utility
#include<vector>

static const int DEFAULT_X_LENGHT = 5;
static const int DEFAULT_Y_LENGHT = 5;
//class which models map in game
class Map
{
private:
	std::vector<MapField> m_mapFields; // use of vector because low memory consumption and fast random acces 
	int m_xLenght;
	int m_yLenght;



public:
	Map();
	//full constructor
	template<typename INT>
	Map(INT&& p_xLenght, INT&& p_yLenght) :
		m_xLenght(std::forward<INT>(p_xLenght)),
		m_yLenght(std::forward<INT>(p_yLenght))
	{}


	//getters and setters
	std::vector<MapField>& getFieldsRef();
	int getXLenght() const;
	int getYlenght() const;

	template<typename T>
	void setFields(T&& p_fieldsToSet)
	{
		m_mapFields = std::forward<T>(p_fieldsToSet);
	}



	//added functionality
	void print() const;

	void populateMap();











};



#endif
#include"Object.h"
#include<iostream>
#include"BufferManipulation.h"

using std::cout;		using std::endl;


Object::Object() :Object(OBJ_DEFAULT_OBJ_NAME, OBJ_DEFAULT_STR_ADD, OBJ_DEFAULT_AGI_ADD, OBJ_DEFAULT_MAX_HEALTH_ADD, OBJ_DEFAULT_X_POS, OBJ_DEFAULT_Y_POS){}

std::string Object::getObjName() const
{
	return m_objName;
}

int Object::getObjStrAdd()const
{
	return m_strAdd;
}

int Object::getObjAgiAdd()const
{
	return m_agiAdd;
}

int Object::getObjmaxHealthAdd()const
{
	return m_maxHealthAdd;
}

int Object::getObjXPos()const
{
	return m_xPos;
}

int Object::getObjYPos()const
{
	return m_yPos;
}

void Object::print() const
{
	/*cout << "Object name: " << m_objName << " STR add: " << m_strAdd << " AGI add: " << m_agiAdd << " MAX_HEALTH add: " << m_maxHealthAdd << endl;
	cout << "Object X POS: " << m_xPos << " Object Y POS: " << m_yPos << endl;*/

	cout << "		Object: " << m_objName << "			Bonuses: " << "+" << m_strAdd << " STR " << "+" << m_agiAdd << " AGI " << "+" << m_maxHealthAdd << " MAX HEALTH		";
	if (m_yPos == -1)
	{
		cout << "IS EQUIPPED." << endl;
	}
	else
	{
		cout << endl;
	}
}

void Object::saveObjectInBinary(File_handle& p_fileToSave)const
{
	p_fileToSave.writeCharBuffer(m_objName);
	p_fileToSave.writeInt(m_strAdd);
	p_fileToSave.writeInt(m_agiAdd);
	p_fileToSave.writeInt(m_maxHealthAdd);
	p_fileToSave.writeInt(m_xPos);
	p_fileToSave.writeInt(m_yPos);
}

void Object::loadObjectFromBuffer(int p_nameSize, int& p_offset, char* p_buffer)
{
	m_objName = getStringFromBuffer(p_buffer, p_offset, p_nameSize);
	m_strAdd = getIntfromBuffer(p_buffer, p_offset);
	m_agiAdd = getIntfromBuffer(p_buffer, p_offset);
	m_maxHealthAdd = getIntfromBuffer(p_buffer, p_offset);
	m_xPos = getIntfromBuffer(p_buffer, p_offset);
	m_yPos = getIntfromBuffer(p_buffer, p_offset);
}
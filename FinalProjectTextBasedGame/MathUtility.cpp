// implementation of math functionality
//return a random integer in range from low to high
#include<cstdlib>
#include"MathUtility.h"
#include"Enums.h"

//complexity O(c)
int Random(int low, int high)
{		//generates random number betveen 0 and max value of int	
	return low + rand() % ((high + 1) - low);
}

//complexity O(c)
int getRandCoord()
{
	return Random(COORD_MIN, COORD_MAX);
}
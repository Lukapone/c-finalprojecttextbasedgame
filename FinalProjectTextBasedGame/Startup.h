#ifndef STARTUP_H
#define STARTUP_H
#include"LoginInfo.h"
#include<string>

static const char FILE_NAME_BUILDINGS[] { "allBuildings.bin"};
static const char FILE_NAME_ENEMIES[] { "allEnemies.bin"};
static const char FILE_NAME_OBJECTS[] { "allObjects.bin"};
static const char FILE_NAME_PLAYER[] { "aPlayer.bin"};
static const char FOLDER_FOR_SAVE[]{ "playersSaves/" };

//startup functions of the game
bool startFromSave(LoginInfo* p_loggedPlayer);
bool startNewGame(LoginInfo* p_loggedPlayer);
bool startNewGamePopulate();
void intro();


























#endif
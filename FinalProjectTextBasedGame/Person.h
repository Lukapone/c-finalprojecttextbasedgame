#ifndef PERSON_H
#define PERSON_H

#include<string>
#include<vector>
#include"Map.h"
#include"Inventory.h"
#include"File_handle.h"
#include"Building.h"

//default values for person
static const char DEFAULT_NAME[] { "noName" };
static const int DEFAULT_STR = 0;
static const int DEFAULT_AGI = 0;
static const int DEFAULT_CUR_HEALTH = 0;
static const int DEFAULT_MAX_HEALTH = 0;
static const int DEFAULT_LEVEL = 0;
static const int DEFAULT_X_POS = 0;
static const int DEFAULT_Y_POS = 0;
static MapField* DEFAULT_POS_PTR = nullptr;
//class which models person in game
class Person
{
private:
	virtual void do_print() const  = 0;
	virtual void do_saveBinary(File_handle& p_fileToSave) const = 0;
	virtual void do_loadFromBuffer(char* p_buffer, int& p_offset) = 0;
	virtual void do_loadFromFile(File_handle& p_fileToLoad) = 0;

	Person& operator=(const Person& p_other);//lock down no slicing allowed
protected://protected for easy acces
	std::string m_name;
	int m_strenght;
	int m_agility;
	int m_currentHealth;
	int m_maxHealth;
	int m_level;
	int m_xPos;
	int m_yPos;
	MapField* m_positionPtr; //i used pointer to mapfield for easy acces and manipulation
	Inventory m_inventory;

	
	

public:
	Person();

	//full constructor
	template<typename STR, typename INT>
	Person(STR&& p_name, INT&& p_strenght, INT&& p_agility, INT&& p_currentHealth,INT&& p_maxHealth, INT&& p_level,MapField* p_positionPtr,INT&& p_xPos,INT&& p_yPos) :
		m_name(std::forward<STR>(p_name)),
		m_strenght(std::forward<INT>(p_strenght)),
		m_agility(std::forward<INT>(p_agility)),
		m_currentHealth(std::forward<INT>(p_currentHealth)),
		m_maxHealth(std::forward<INT>(p_maxHealth)),
		m_level(std::forward<INT>(p_level)),
		m_xPos(std::forward<INT>(p_xPos)),
		m_yPos(std::forward<INT>(p_yPos)),
		m_positionPtr(p_positionPtr)
	{}

	virtual ~Person();

	//getters and setters
	std::string getName() const;
	int getStrenght() const;
	int getAgility() const;
	int getCurrentHealth() const;
	int getMaxHealth() const;
	int getLevel() const;
	int getXPos()const;
	int getYPos()const;
	MapField getPosition() const;
	MapField* getPositionPtr() const;
	Inventory getInventory() const;

	void setPositionPtr(MapField* p_mapFieldPtr);
	template<typename T>
	void setXPos(T&& p_xPos)
	{
		m_xPos = std::forward<T>(p_xPos);
	}
	template<typename T>
	void setYPos(T&& p_yPos)
	{
		m_yPos = std::forward<T>(p_yPos);
	}

	template<typename T>
	void setName(T&& p_name)
	{
		m_name = std::forward<T>(p_name);
	}

	template<typename T>
	void setStrenght(T&& p_strenght)
	{
		m_strenght = std::forward<T>(p_strenght);
	}

	template<typename T>
	void setAgility(T&& p_agility)
	{
		m_agility = std::forward<T>(p_agility);
	}

	template<typename T>
	void setCurrentHealth(T&& p_currentHealth)
	{
		m_currentHealth = std::forward<T>(p_currentHealth);
	}

	template<typename T>
	void setMaxHealth(T&& p_maxHealth)
	{
		m_maxHealth = std::forward<T>(p_maxHealth);
	}

	template<typename T>
	void setLevel(T&& p_level)
	{
		m_level = std::forward<T>(p_level);
	}



	//added functions
	void addStrenght(int p_toAdd);
	void addAgility(int p_toAdd);
	void addCurrentHealth(int p_toAdd);
	void addMaxHealth(int p_toAdd);
	void healTotaly();
	void respawnRandomLocation(Map& p_map);


	void print() const;
	void printInventory()const;
	void printMapFieldYouStandOn()const;
	void setPositionPtrFromMap(Map& p_myMap);
	bool goNorth(Map& p_myMap);
	bool goSouth(Map& p_myMap);
	bool goEast(Map& p_myMap);
	bool goWest(Map& p_myMap);
	bool visitBuilding();
	void getKeyFromBuilding(Building* p_building);
	void getKeyFromBuildings(std::vector<Building*>& p_buildings);
	void setXandYPosFromPtr();

	bool pickUpIfExists(const std::vector<std::string>& p_sentence);
	bool dropIfExists(const std::vector<std::string>& p_sentence);
	void equipIfExists(const std::vector<std::string>& p_sentence);
	void unequipIfExists(const std::vector<std::string>& p_sentence);
	bool pickUpPtr(Object* p_toPick);
	void getObjectBonuses(Object* p_getBonuses);
	void removeObjectBonuses(Object* p_getBonuses);

	bool takeDamage(int p_damageToTake);


	void saveToFile(const std::string& p_fileName)const;
	void loadFromFile(const std::string& p_fileName);
	void saveInBinary(File_handle& p_fileToSave)const;
	void loadFromBuffer(int p_nameSize, int& p_offset, char* p_buffer);

};





#endif
#include"LoginMenu.h"
#include<iostream>
#include"UserInputUtility.h"
#include"LoadMenu.h"
#include<memory>

using std::cin;		using std::cout;
using std::endl;	using std::string;
using std::vector;

enum MenuOptions
{
	LOGIN = 1, CREATE_NEW_LOGIN = 2, QUIT = 3
	
};

bool printLoginMenu(LoadedLogins& p_allLogins)
{
	cout << "Welcome to the login menu of Text Castle Game!" << endl;
	cout << "Press 1 to login" << endl;
	cout << "      2 to create new login" << endl;
	cout << "      3 to quit" << endl;

	return getInputLoginMenu(p_allLogins);
}

//function which takes input from player for login and checks if is correct or creates new one
//complexity depends on choice worst is order(n)
bool getInputLoginMenu(LoadedLogins& p_allLogins)
{

	int userChoice = userInputInt();

	if (userChoice == LOGIN)
	{
		LoginInfo* loggedPlayer{ nullptr };//create pointer to logged players info which will change after the function
		if (p_allLogins.confirmLogin(loggedPlayer))//int the function i passed the reference to the pointer and this will change it to what i need
		{
			
			//start game here
			cout << "Username and password correct starting game." << endl;
			while (printLoadMenu(loggedPlayer) == false){}//now i can pass pointer pointing to what i want to change in LoginInfo 
			return true;
		}
		else
		{
			cout << "Username or password incorrect." << endl;
		}
	}
	else if (userChoice == CREATE_NEW_LOGIN)
	{
		p_allLogins.addNewLogin();
		p_allLogins.saveAllLoginsIntoFile("logins/allLogins.bin");
	}
	else if (userChoice == QUIT)
	{
		return true;
	}
	else
	{
		cout << "Invalid input" << endl;
		cin.clear();//clears the buffer
		cin.ignore(80, '\n'); //ignores characters until end of line
	}

	return false;


}

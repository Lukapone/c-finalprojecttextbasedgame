#include"LoadedBuildings.h"
#include<iostream>
#include"File_handle.h"
#include<memory>
#include"BufferManipulation.h"
#include"MathUtility.h"
#include"Enums.h"
#include"UserInputUtility.h"
using std::unique_ptr;//http://msdn.microsoft.com/en-us/library/hh279676.aspx
using std::make_unique;	
using std::vector;		using std::cout;
using std::endl;		using std::cin;
using std::string;

const static unsigned char METADATA_BUILDING_LENGHT = 2;//number of integers which stores string lenghts


LoadedBuildings::LoadedBuildings()
{
}

vector<Building>& LoadedBuildings::getBuildingsRef()
{
	return m_allBuildings;
}

void LoadedBuildings::print() const
{
	for (const auto& building : m_allBuildings)
	{
		building.print();
	}
}

void LoadedBuildings::populate()
{
	Building b1("Castle Lavina", R"(	This castle was build to protect the people of the Lavina town but 
		the town burned down and now it is the reminder of the horrible things that happened here.)", 0, 0);

	Building b2("Castle Anora", R"(		This was once the crown jewel of the anora province until vampires attacked 
		this place and took over it. Now it serves as protection from sun during days and celebration place during nights.)",0, 1);

	Building b3("Castle Corvinus", R"(		Castle corvinus grew in the richest province. The provinces wetly king
		made it into beautiful building with golden statues and towers.)", 0, 2);

	Building b4("Dark Tower", R"(		The legend says that old and powerful wizard lived in this tower.
		If you want to ever enter it you must consume special elixir made by the wizard and then you can see the secret entrance.)", 0, 3);

	Building b5("Water Mill", R"(		This entirely made from wood mill serves people well for grinding the barley into flower.
		Some people also says that they see ghosts floating around but who know if its true.)",1,0);

	Building b6("Woodcutters House", R"(	The woodcutters lives here. There is a lot of bones around the garden and 
		sign in front of the house beware werewolf.)", 1, 1);

	Building b7("Lighthouse", R"(	The old stone lighthouse with broken windows. 
		Some people says that if you see light shining in the night in it, the orcs have meeting in the forest nearby.)", 1, 2);

	m_allBuildings.push_back(std::move(b1));
	m_allBuildings.push_back(std::move(b2));
	m_allBuildings.push_back(std::move(b3));
	m_allBuildings.push_back(std::move(b4));
	m_allBuildings.push_back(std::move(b5));
	m_allBuildings.push_back(std::move(b6));
	m_allBuildings.push_back(std::move(b7));
}

//function which will let the user create a building of his choice

void LoadedBuildings::createBuilding(Map& p_myMap)
{
	int xLocation{ getRandCoord() };
	int yLocation{ getRandCoord() };
	Building created(userInputStrLineWithText("Enter name of your building."),
		userInputStrLineWithText("Enter description of your building."), xLocation, yLocation);
	cout << "You created your own building and it is located at: " << xLocation << " in x coord and " << yLocation << " in y coord." << endl;
	//this part adds it into vector and map
	m_allBuildings.push_back(std::move(created));
	int offsetInVector{ (created.getBuildingXPos()*p_myMap.getYlenght()) + created.getBuildingYPos() };
	p_myMap.getFieldsRef()[offsetInVector].addBuildingPtr(&m_allBuildings[m_allBuildings.size() - 1]);
}

//function which will put all buildings pointers into map 
//complexity is O(n) more buildings longer it takes
void LoadedBuildings::putBuildingPtrsIntoMap(Map& p_myMap)
{
	int offsetInVector{ 0 };
	for (auto& building : m_allBuildings)
	{
		offsetInVector=(building.getBuildingXPos()*p_myMap.getYlenght())+building.getBuildingYPos();//formula to get offset in vector for location
		p_myMap.getFieldsRef()[offsetInVector].addBuildingPtr(&building);
	}
}

//function which will create the vector of lenghts of the strings from building class for each building
//complexity O(n)
vector<int> LoadedBuildings::createMetaDataForFile() const
{
	vector<int> allMetadata{};
	allMetadata.push_back(m_allBuildings.size());//push the number of buildings

	for (const auto& building : m_allBuildings)
	{
		allMetadata.push_back(building.getBuildingName().size());
		allMetadata.push_back(building.getBuildingDescription().size());
	}

	return allMetadata;

}

//function which will save all buildings into binary file
//complexity O(n) 
void LoadedBuildings::saveAllBuildingsIntoFile(const string& p_filename)const
{
	File_handle fileForSave(p_filename, "wb");
	fileForSave.writeIntegers(this->createMetaDataForFile());
	for (auto& building : m_allBuildings)
	{
		building.saveBuildingInBinary(fileForSave);
	}
}


//function for loading all buildings from file 
//complexity O(n)
void LoadedBuildings::loadAllBuildingsFromFile(const string& p_filename)
{
	File_handle fileForLoad(p_filename, "rb");
	auto loadedBuffer = fileForLoad.readCharBufferWithUniquePtr();

	int offset{ 0 };
	int numOfBuildings{ getIntfromBuffer(loadedBuffer.get(), offset) };
	vector<int> metadata{ getIntegersfromBuffer(loadedBuffer.get(), offset, numOfBuildings*METADATA_BUILDING_LENGHT) };


	Building loadedOne;
	int counter{ 0 };
	for (int i = 0; i != numOfBuildings; i++)
	{
		loadedOne.loadBuildingFromBuffer(metadata[counter], metadata[counter+1], offset, loadedBuffer.get());
		counter = counter + METADATA_BUILDING_LENGHT;
		m_allBuildings.push_back(std::move(loadedOne));
	}

}
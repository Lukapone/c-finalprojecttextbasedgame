#ifndef FILE_HANDLE_H
#define FILE_HANDLE_H
#include<stdio.h>
#include<string>
#include<memory>
#include<vector>
//file handle class which helps with taking care of file resources

class File_handle
{
private:
	FILE* m_file;
	//errno_t errorInt;

public:
	
	File_handle(const char* p_fileName, const char* p_mode);//modes r-read w-write a-append b-binary +-update

	File_handle(const std::string& p_fileName, const char* p_mode);//modes r-read w-write a-append b-binary +-update

	~File_handle();//destructor gets called even if some exceptions occures

	FILE* getFilePtr();

	void writeCharBuffer(std::string p_toWrite);

	void flushToFile();

	void writeInt(int p_intToWrite);
	void writeIntegers(std::vector<int>& p_intsToWrite);
	void writeChar(char p_charToWrite);

	//void readCharBuffer();
	std::unique_ptr<char[]> File_handle::readCharBufferWithUniquePtr();//returns unique ptr for my buffer respective whole loaded file that i can process 

	int readIntFromFileAtPointer();
	std::string readStringFromFileAtPointer(int p_numCars);
};



#endif
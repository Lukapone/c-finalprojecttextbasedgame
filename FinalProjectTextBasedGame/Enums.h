#ifndef ENUMS_H
#define ENUMS_H

//default experience boundries for levels
enum expBoundries
{
	LEVEL1 = 200, LEVEL2 = 300, LEVEL3 = 500, LEVEL4 = 600, LEVEL5 = 700, LEVEL6 = 800, LEVEL7 = 900, LEVEL8 = 1000, LEVEL9 = 1200
};

enum healthPerLVL
{
	HEALTHBONUS = 30
};

//boundries for map coords of default map 5x5 fields
enum mapBoundries
{
	COORD_MIN = 0, COORD_MAX = 4
};

#endif
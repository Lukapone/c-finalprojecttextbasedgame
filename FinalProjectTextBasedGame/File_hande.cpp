
#include"File_handle.h"
#include<iostream>
using std::cout;		using std::endl;		using std::cin;
using std::string;		using std::unique_ptr;//http://msdn.microsoft.com/en-us/library/hh279676.aspx
using std::make_unique;	using std::vector;

File_handle::File_handle(const char* p_fileName, const char* p_mode)//modes r-read w-write a-append b-binary +-update
{
	if (fopen_s(&m_file, p_fileName, p_mode) != 0)
	{
		perror("Error opening file");		
		
	}
}
File_handle::File_handle(const string& p_fileName, const char* p_mode)//modes r-read w-write a-append b-binary +-update
{
	if( fopen_s(&m_file, p_fileName.c_str(), p_mode)!=0)
	{
		cout << p_fileName << endl;
		perror("Error opening file");
	}	
}
File_handle::~File_handle()//destructor gets called even if some exceptions occures
{
	
	fclose(m_file);
}

void File_handle::writeChar(char p_charToWrite)
{
	int numWritten = fwrite(&p_charToWrite, sizeof(char), sizeof(char), m_file);
	//cout << "written:"<<numWritten << endl;
}

//function for writing string into binary file
void File_handle::writeCharBuffer(std::string p_toWrite)
{
	int numWritten=fwrite(p_toWrite.c_str(), sizeof(char), p_toWrite.size(), m_file);
	//cout << "written:"<<numWritten << endl;
}

//function for writing int to binary file
void File_handle::writeInt(int p_intToWrite)
{
	int numWritten = fwrite(&p_intToWrite, sizeof(int), 1, m_file);
	//cout << "written:" << numWritten << endl;
}

//function for writing ints to binary file
void File_handle::writeIntegers(vector<int>& p_intsToWrite)
{
	int numWritten = fwrite(&p_intsToWrite[0], sizeof(int),p_intsToWrite.size() , m_file);
	//cout << "written:" << numWritten << endl;
}

void File_handle::flushToFile()
{
	fflush(m_file);
}

FILE* File_handle::getFilePtr()
{
	return m_file;
}

//version from internet usinf malloc and free

//void File_handle::readCharBuffer()
//{
//	long lSize;
//	char * buffer;
//	size_t result;
//
//	// obtain file size:
//	fseek(m_file, 0, SEEK_END);
//	lSize = ftell(m_file);
//	rewind(m_file);
//
//	// allocate memory to contain the whole file:
//	buffer = (char*)malloc(sizeof(char)*lSize);
//	if (buffer == nullptr) { fputs("Memory error", stderr); exit(2); }
//
//	// copy the file into the buffer:
//	result = fread_s(buffer, lSize, 1, lSize, m_file);
//	if (result != lSize) { fputs("Reading error", stderr); exit(3); }
//
//	/* the whole file is now loaded in the memory buffer. */
//	for (int i = 0; i != lSize; i++)
//	{
//		cout << buffer[i] << endl;
//	}
//	// terminate
//	free(buffer);
//
//}

//function which read int from file at the pointer of the file
int File_handle::readIntFromFileAtPointer()
{
	int toRet{ 0 };
	int numRead = fread_s(&toRet, sizeof(int), sizeof(int), 1, m_file);
	//cout << "You made: " << numRead << " read." << endl;
	return toRet;
}

//function which read string from file at the pointer of the file
string File_handle::readStringFromFileAtPointer(int p_numCars)
{
	unique_ptr<char[]> myUniqueBuffer = make_unique<char[]>(p_numCars);//make unique pinter easier memory management as fast as normal new and delete
	
	int numRead = fread_s(myUniqueBuffer.get(), p_numCars, sizeof(char), p_numCars, m_file);
	//cout << "You made: " << numRead << " read." << endl;
	string toRet{};
	toRet.reserve(p_numCars);
	for (int i = 0; i != p_numCars; i++)
	{
		toRet.push_back(myUniqueBuffer[i]);
	}
	return toRet;
}

//function which creates buffer for whole file as unique ptr and returns that pointer for further use
unique_ptr<char[]> File_handle::readCharBufferWithUniquePtr()
{
	
	fseek(m_file, 0, SEEK_END);
	int bufferSize = ftell(m_file);
	//cout << "The file lenght is :" << bufferSize << endl;;
	rewind(m_file);
	//this creates unique pointer to my array 
	unique_ptr<char[]> myUniqueBuffer = make_unique<char[]>(bufferSize);

	//this reads whole file into buffer
	int numRead = fread_s(myUniqueBuffer.get(), bufferSize, sizeof(char), bufferSize, m_file);
	//cout << "You made " << numRead << " reads." << endl;

	return myUniqueBuffer;
}


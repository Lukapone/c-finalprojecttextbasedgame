#ifndef STRINGUTILITY_H
#define STRINGUTILITY_H
//this is string functionality interface to the user
//here are some functions that help me work with strings
#include<string>
#include<vector>


std::string findCharAndReplace(std::string forReplace, char toReplace);
void printInLines(std::string toPrint, char newLine);
std::string toLowerCase(const std::string& toCast);
std::string checkAndReplace(std::string forReplace, std::string whatToReplace, std::string replaceWith);
std::vector<std::string> removeAllByname(std::vector<std::string> toRemove, std::string forRemove);

#endif
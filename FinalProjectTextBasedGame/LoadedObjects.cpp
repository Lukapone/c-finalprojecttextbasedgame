#include"LoadedObjects.h"
#include"BufferManipulation.h"
#include<iostream>
#include"MathUtility.h"

using std::vector;		using std::cout;
using std::endl;		using std::cin;
using std::string;

LoadedObjects::LoadedObjects()
{
}

vector<Object>& LoadedObjects::getObjectsRef()
{
	return m_allObjcets;
}

void LoadedObjects::print() const
{
	for (const auto& object : m_allObjcets)
	{
		object.print();
	}
}

void LoadedObjects::populate()
{
	//STR  AGI  HEALTH
	Object o1("Sword", 2, 9, 55, 0, 0);
	Object o2("BroadSword", 3, 8, 55, getRandCoord(), getRandCoord());
	Object o3("SteelSword", 4, 7, 55, getRandCoord(), getRandCoord());
	Object o4("DamascusSword", 5, 6, 55, getRandCoord(), getRandCoord());
	Object o5("AlbionSword", 6, 5, 45, getRandCoord(), getRandCoord());
	Object o6("RegentSword", 7, 4, 45, getRandCoord(), getRandCoord());
	Object o7("RangerSword", 8, 3, 45, getRandCoord(), getRandCoord());
	Object o8("JarlSword", 9, 2, 45, getRandCoord(), getRandCoord());

	Object o9("Shield", 1, 9, 155,0, 1);
	Object o10("BroadShield", 2, 8, 155, getRandCoord(), getRandCoord());
	Object o11("SteelShield", 3, 7, 155, getRandCoord(), getRandCoord());
	Object o12("DamascusShield", 4, 6, 155, getRandCoord(), getRandCoord());
	Object o13("AlbionShield", 5, 5, 155, getRandCoord(), getRandCoord());
	Object o14("RegentShield", 6, 4, 155, getRandCoord(), getRandCoord());
	Object o15("RangerShield", 7, 3, 155, getRandCoord(), getRandCoord());
	Object o16("JarlShield", 8, 2, 155, getRandCoord(), getRandCoord());

	Object o17("Staff", 3, 22, 80, 0, 2);
	Object o18("BroadStaff", 3, 23, 80, getRandCoord(), getRandCoord());
	Object o19("SteelStaff", 3, 24, 80, getRandCoord(), getRandCoord());
	Object o20("DamascusStaff", 3, 25, 80, getRandCoord(), getRandCoord());
	Object o21("AlbionStaff", 3, 26, 80, getRandCoord(), getRandCoord());
	Object o22("RegentStaff", 3, 27, 80, getRandCoord(), getRandCoord());
	Object o23("RangerStaff", 3, 28, 80, getRandCoord(), getRandCoord());
	Object o24("JarlStaff", 3, 29, 80, getRandCoord(), getRandCoord());
	Object o25("KingsCape", 33, 40, 800, -3, -3);//i set the position to used key so no problem when i need to use it later

	Object ok1("Key", 0, 0, 0, -2, 0);
	Object ok2("Key", 0, 0, 0, -2, 1);
	Object ok3("Key", 0, 0, 0, -2, 2);
	Object ok4("Key", 0, 0, 0, -2, 3);
	Object ok5("Key", 0, 0, 0, -2, 4);
	Object ok6("Key", 0, 0, 0, -2, 5);
	Object ok7("Key", 0, 0, 0, -2, 6);



	m_allObjcets.push_back(std::move(o1));
	m_allObjcets.push_back(std::move(o2));
	m_allObjcets.push_back(std::move(o3));
	m_allObjcets.push_back(std::move(o4));
	m_allObjcets.push_back(std::move(o5));
	m_allObjcets.push_back(std::move(o6));
	m_allObjcets.push_back(std::move(o7));
	m_allObjcets.push_back(std::move(o8));
	m_allObjcets.push_back(std::move(o9));
	m_allObjcets.push_back(std::move(o10));
	m_allObjcets.push_back(std::move(o11));
	m_allObjcets.push_back(std::move(o12));
	m_allObjcets.push_back(std::move(o13));
	m_allObjcets.push_back(std::move(o14));
	m_allObjcets.push_back(std::move(o15));
	m_allObjcets.push_back(std::move(o16));
	m_allObjcets.push_back(std::move(o17));
	m_allObjcets.push_back(std::move(o18));
	m_allObjcets.push_back(std::move(o19));
	m_allObjcets.push_back(std::move(o20));
	m_allObjcets.push_back(std::move(o21));
	m_allObjcets.push_back(std::move(o22));
	m_allObjcets.push_back(std::move(o23));
	m_allObjcets.push_back(std::move(o24));
	m_allObjcets.push_back(std::move(o25));

	m_allObjcets.push_back(std::move(ok1));
	m_allObjcets.push_back(std::move(ok2));
	m_allObjcets.push_back(std::move(ok3));
	m_allObjcets.push_back(std::move(ok4));
	m_allObjcets.push_back(std::move(ok5));
	m_allObjcets.push_back(std::move(ok6));
	m_allObjcets.push_back(std::move(ok7));
}

//divide all of the objects between map, player, building
//complexity O(n)
void LoadedObjects::putObjectsPtrsIntoMap(Map& p_myMap, Player& p_player, LoadedBuildings& allBuildings)
{
	int offsetInVector{ 0 };
	for (auto& object : m_allObjcets)
	{
		if (object.getObjXPos() == IS_EQUIPPED)
		{
			p_player.pickUpPtr(&object);
		}
		else if (object.getObjXPos() == IS_KEY)
		{
			allBuildings.getBuildingsRef()[object.getObjYPos()].setKeyPtr(&object);
		}
		else if (object.getObjXPos() == USED_KEY)
		{
			//this are all used keys i can decide to put them back after certain amount of time passed and let the player to build another building
		}
		else
		{
			offsetInVector = (object.getObjXPos()*p_myMap.getYlenght()) + object.getObjYPos();//formula to get offset in vector for location
			p_myMap.getFieldsRef()[offsetInVector].addObjectPtr(&object);//adds all objects into field 
		}	
	}
}

//function which will create the vector of lenghts of the strings from object class for each object
//complexity O(n)
vector<int> LoadedObjects::createMetaDataForFile() const
{
	vector<int> allMetadata{};
	allMetadata.push_back(m_allObjcets.size());//push the number of buildings

	for (const auto& object : m_allObjcets)
	{
		allMetadata.push_back(object.getObjName().size());
	}

	return allMetadata;

}

//function which will save all objects into binary file
//complexity O(n) 
void LoadedObjects::saveAllObjectsIntoFile(const string& p_filename)const
{
	File_handle fileForSave(p_filename, "wb");
	fileForSave.writeIntegers(this->createMetaDataForFile());
	for (const auto& object : m_allObjcets)
	{
		object.saveObjectInBinary(fileForSave);
	}
}

//function for loading all objects from file 
//complexity O(n)
void LoadedObjects::loadAllObjectsFromFile(const string& p_filename)
{
	File_handle fileForLoad(p_filename, "rb");
	auto loadedBuffer = fileForLoad.readCharBufferWithUniquePtr();

	int offset{ 0 };
	int numOfObjects{ getIntfromBuffer(loadedBuffer.get(), offset) };
	vector<int> metadata{ getIntegersfromBuffer(loadedBuffer.get(), offset, numOfObjects) };

	m_allObjcets.reserve(numOfObjects);
	Object loadedOne;
	int counter{ 0 };
	for (int i = 0; i != numOfObjects; i++)
	{
		loadedOne.loadObjectFromBuffer(metadata[i], offset, loadedBuffer.get());
		m_allObjcets.push_back(std::move(loadedOne));
	}

}
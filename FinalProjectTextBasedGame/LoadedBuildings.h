#ifndef LOADEDBUILDINGS_H
#define LOADEDBUILDINGS_H

#include"Building.h"
#include"Map.h"
#include<string>
//this class keeps track of all buildings in game

class LoadedBuildings
{
private:
	std::vector<Building> m_allBuildings; //use of vector because low memory consumption and fast random acces also the buildings doesn't need to be in order



public:
	LoadedBuildings();

	template<typename V>
	LoadedBuildings(V&& p_allBuildings) :
		m_allBuildings(std::forward<V>(p_allBuildings)){}


	//getters and setters
	std::vector<Building>& getBuildingsRef();

	template<typename T>
	void setBuildings(T&& p_buildingsToSet)
	{
		m_allBuildings = std::forward<T>(p_buildingsToSet);
	}



	//added functionality
	void print() const;
	void populate();
	void createBuilding(Map& p_myMap);

	void putBuildingPtrsIntoMap(Map& p_myMap);


	//save and load functionality
	std::vector<int> createMetaDataForFile() const;
	void saveAllBuildingsIntoFile(const std::string& p_filename)const;
	void loadAllBuildingsFromFile(const std::string& p_filename);

};


#endif
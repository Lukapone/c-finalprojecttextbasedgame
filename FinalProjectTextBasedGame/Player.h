#ifndef PLAYER_H
#define PLAYER_H
//this is enemy class interface to the user
//enemy class that extends player class

#include"Person.h"
#include"Map.h"
static const int DEFAULT_START_EXP = 0;
static const int SWING_BONUS_STR = 10;
static const int SWING_BONUS_AGI = 30;
//class which models player in game and inherits from person

class Player : public Person
{
private:
	int m_currentExperience;

	virtual void do_print()const final override;//final helps compiler with virtual function calls
	virtual void do_saveBinary(File_handle& p_fileToSave)const final override;
	virtual void do_loadFromBuffer(char* p_buffer, int& p_offset)final override;
	virtual void do_loadFromFile(File_handle& p_fileToLoad)final override;
public:
	Player();

	template<typename STR, typename INT>
	Player(STR&& p_name, INT&& p_strenght, INT&& p_agility, INT&& p_currentHealth, INT&& p_maxHealth, INT&& p_level, INT&& p_experienceReward, MapField* p_positionPtr, INT&& p_xPos, INT&& p_yPos) :
		Person(std::forward<STR>(p_name),
		std::forward<INT>(p_strenght),
		std::forward<INT>(p_agility),
		std::forward<INT>(p_currentHealth),
		std::forward<INT>(p_maxHealth),
		std::forward<INT>(p_level),
		p_positionPtr,
		std::forward<INT>(p_xPos),
		std::forward<INT>(p_yPos)),
		m_currentExperience(std::forward<INT>(p_experienceReward))
	{}

	//added functions
	bool useAllKeyes();
	bool fightIfExists(Map& p_myMap);
	void fightSimulation(Enemy* p_toFight,Map& p_myMap);
	bool swingEnemy(Enemy* p_eToSwing);
	bool getHitByEnemy(Enemy* p_eToByHitBy);
	void addExperience(int p_expToAdd);
	void checkForLevelUp();
	void addLevel();
	bool addBonusStrength() const;
};



#endif
#ifndef INVENTORY_H
#define INVENTORY_H
#include<string>
#include"Object.h"
#include<vector>
#include<utility>
//this is the inventory class which holds objects in a vector

class Person;
static const int MAX_EQUIPPED = 2;
static const int DEFAULT_NUM_OF_EQUIPPED_ITEMS = 0;
static const int DEFAULT_INIT_SIZE = 10;
static const int IS_EQUIPPED = -1;
static const int IS_KEY = -2;
static const int USED_KEY = -3;

class Inventory
{
private:
	std::vector<Object*> m_objcetsPtrs; //i used pointers to object for fast and easy manipulation with vector and low memory consumption with swap for fast removal 
	//(no need for objects to be in order)
	int m_numOfEquippedObjs;
public:

	Inventory();
	Inventory(const int p_initSize);
	//full constructor
	template<class V>
	Inventory(V&& p_objectsPtrs) :
		m_objcetsPtrs(std::forward(p_objectsPtrs))
	{}

	//added functionality
	void addToInventoryPtr(Object* p_toAdd);
	std::vector<Object*>& getAllObjectsPtrs();
	Object* getObjPtrByNameAndRemove(const std::vector<std::string>& p_sentence);
	Object* getRandomObjPtrAndRemove(Person* p_player);
	void print() const;
	bool setToEquipped(const std::vector<std::string>& p_sentence,Person* p_player);
	bool removeFromEquipped(const std::vector<std::string>& p_sentence, Person* p_player);
	bool useAllKeyes();
};


#endif
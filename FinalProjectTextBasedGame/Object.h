#ifndef OBJECT_H
#define OBJECT_H

#include<string>
#include<utility>
#include"File_handle.h"

//default values for object
static const char OBJ_DEFAULT_OBJ_NAME[] {"noObjName"};
static const int OBJ_DEFAULT_STR_ADD = 0;
static const int OBJ_DEFAULT_AGI_ADD = 0;
static const int OBJ_DEFAULT_MAX_HEALTH_ADD = 0;
static const int OBJ_DEFAULT_X_POS = 0;
static const int OBJ_DEFAULT_Y_POS = 0;
//class which models object in game
class Object
{
private:
	std::string m_objName;
	int m_strAdd;
	int m_agiAdd;
	int m_maxHealthAdd;
	int m_xPos;
	int m_yPos;

public:
	Object();

	template<typename STR, typename INT>
	Object(STR&& p_objName, INT&& p_strAdd, INT&& p_agiAdd, INT&& p_maxHealthAdd,INT&& p_xPos,INT&& p_yPos) :
		m_objName(std::forward<STR>(p_objName)),
		m_strAdd(std::forward<INT>(p_strAdd)),
		m_agiAdd(std::forward<INT>(p_agiAdd)),
		m_maxHealthAdd(std::forward<INT>(p_maxHealthAdd)),
		m_xPos(std::forward<INT>(p_xPos)),
		m_yPos(std::forward<INT>(p_yPos))
	{}

	//getters and setters
	std::string getObjName() const;
	int getObjStrAdd()const;
	int getObjAgiAdd()const;
	int getObjmaxHealthAdd()const;
	int getObjXPos()const;
	int getObjYPos()const;

	template<typename T>
	void setObjXPos(T&& p_objXpos)
	{
		m_xPos = std::forward<T>(p_objXpos);
	}
	template<typename T>

	void setObjYPos(T&& p_objYpos)
	{
		m_yPos = std::forward<T>(p_objYpos);
	}
	template<typename T>
	void setObjName(T&& p_objName)
	{
		m_objName = std::forward<T>(p_objName);
	}
	template<typename T>
	void setStrAdd(T p_strAdd)
	{
		m_strAdd = std::forward<T>(p_strAdd);
	}
	template<typename T>
	void setAgiAdd(T p_agiAdd)
	{
		m_agiAdd = std::forward<T>(p_agiAdd);
	}
	template<typename T>
	void setMaxHealthAdd(T p_maxHealthAdd)
	{
		m_maxHealthAdd = std::forward<T>(p_maxHealthAdd);
	}


	//added functions
	void print() const;

	void saveObjectInBinary(File_handle& p_fileToSave)const;
	void loadObjectFromBuffer(int p_nameSize, int& p_offset, char* p_buffer);



};




#endif
#include"LoadedLogins.h"
#include<iostream>
#include<string>
#include"UserInputUtility.h"
#include"File_handle.h"
#include"BufferManipulation.h"


using std::cout;	using std::endl;
using std::string;	using std::vector;

LoadedLogins::LoadedLogins(){};

//added functionality
void LoadedLogins::print() const
{
	for (const auto& login : m_allLogins)
	{
		login.print();
	}

}

void LoadedLogins::populate()
{
	LoginInfo l1("Peter", "2288");
	LoginInfo l2("Stevo", "hg457");
	LoginInfo l3("Anna", "abc44");
	LoginInfo l4("Merci", "ppp222");
	LoginInfo l5("Ibkiss", "1758a");

	m_allLogins.push_back(l1);
	m_allLogins.push_back(l2);
	m_allLogins.push_back(l3);
	m_allLogins.push_back(l4);
	m_allLogins.push_back(l5);
}

//function which will let the user create the login and checks for duplicate
bool LoadedLogins::addNewLogin()
{

	string name{ userInputStrWithText("Enter new username:") };
	if (this->checkForDuplicate(name))
	{
		cout << "Username already exists, choose different one."<<endl;
		//this->addNewLogin(); //another way to do it if you want him to keep enterring
		return false;
	}
	else
	{
		string password{ userInputStrWithText("Enter new password:") };
		m_allLogins.push_back(std::move(LoginInfo(name, password)));
	}
	cout << "LOGIN ADDED" << endl;
	cout << "Please use it to login into game." << endl;
	return true;

}

//function which will checks the logins for the duplicate and returns true if there is one
//order is O(n) 
bool LoadedLogins::checkForDuplicate(const string& p_toCheck) const
{
	for (const auto& login : m_allLogins)
	{
		if (login.getName() == p_toCheck)
		{
			//name already exists
			return true;
		}
	}
	return false;
}

//function which takes user login and confirms if is correct
//complexity O(n)
bool LoadedLogins::confirmLogin(LoginInfo*& p_loggedPlayer)//reference to pointer 
{
	string name{ userInputStrWithText("Enter username:") };
	string password{ userInputStrWithText("Enter password:") };
	return this->searchAndConfirmLogin(name, password,p_loggedPlayer);
}

//function which searches and confirms login if it exists
//complexity O(n)
bool LoadedLogins::searchAndConfirmLogin(const string& p_loginName, const string& p_password, LoginInfo*& p_loggedPlayer)//reference to pointer
{
	for (auto& login : m_allLogins)
	{
		if (login.getName() == p_loginName && login.getPassword()==p_password)
		{
			//confirmed
			p_loggedPlayer = &login;
			//cout <<"AT LOGIN"<< &login << endl;
			return true;
		}
	}
	
	return false;
}

//function which will create the vector of lenghts of the strings from LoginInfo class for each login
//complexity O(n)
vector<int> LoadedLogins::createMetaDataForFile() const
{
	vector<int> allMetadata{};
	allMetadata.push_back(m_allLogins.size());//push the number of buildings

	for (const auto& login : m_allLogins)
	{
		allMetadata.push_back(login.getName().size());
		allMetadata.push_back(login.getPassword().size());
	}

	return allMetadata;

}

//function which will save all logins into binary file
//complexity O(n) 
void LoadedLogins::saveAllLoginsIntoFile(const string& p_filename)const
{
	File_handle fileForSave(p_filename, "wb");
	fileForSave.writeIntegers(this->createMetaDataForFile());
	for (auto& login : m_allLogins)
	{
		login.saveLoginInBinary(fileForSave);
	}
}

//function for loading all from file 
//complexity O(n)
void LoadedLogins::loadAllLoginsFromFile(const string& p_filename)
{
	File_handle fileForLoad(p_filename, "rb");
	auto loadedBuffer = fileForLoad.readCharBufferWithUniquePtr();

	int offset{ 0 };
	int numOfLogins{ getIntfromBuffer(loadedBuffer.get(), offset) };
	vector<int> metadata{ getIntegersfromBuffer(loadedBuffer.get(), offset, numOfLogins*METADATA_LOGIN_LENGHT) };


	LoginInfo loadedOne;
	int counter{ 0 };
	for (int i = 0; i != numOfLogins; i++)
	{
		loadedOne.loadLoginFromBuffer(metadata[counter], metadata[counter + 1], offset, loadedBuffer.get());
		counter = counter + METADATA_LOGIN_LENGHT;
		m_allLogins.push_back(std::move(loadedOne));
	}

}
#ifndef LOADED_LOGINS_H
#define LOADED_LOGINS_H

#include"LoginInfo.h"
#include<vector>
static const int METADATA_LOGIN_LENGHT = 2;
//class which keeps track of all logins in game

class LoadedLogins
{
private:
	std::vector<LoginInfo> m_allLogins; // use of vector because low memory consumption and fast random acces also the logins doesn't need to be in order


public:
	LoadedLogins();

	template<typename V>
	LoadedLogins(V&& p_allLogins) :
		m_allLogins(std::forward<V>(p_allLogins)){}

	//added functionality
	bool addNewLogin();

	void print() const;
	void populate();

	bool checkForDuplicate(const std::string& p_toCheck) const;
	//bool confirmLogin(LoginInfo* p_logedPlayer);
	//bool searchAndConfirmLogin(const std::string& p_loginName, const std::string& p_password, LoginInfo* p_logedPlayer);
	bool confirmLogin(LoginInfo*& p_logedPlayer);
	bool searchAndConfirmLogin(const std::string& p_loginName, const std::string& p_password, LoginInfo*& p_logedPlayer);
	//file manip
	std::vector<int> createMetaDataForFile() const;
	void saveAllLoginsIntoFile(const std::string& p_filename)const;
	void loadAllLoginsFromFile(const std::string& p_filename);
};






#endif
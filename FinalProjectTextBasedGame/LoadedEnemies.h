#ifndef LOADEDENEMIES_H
#define LOADEDENEMIES_H

#include"Enemy.h"
#include"Map.h"
#include"LoadedObjects.h"
//class which keeps track of all enemies in game

class LoadedEnemies
{
private:
	std::vector<Enemy> m_allEnemies; // use of vector because low memory consumption and fast random acces also the enemies doesn't need to be in order



public:
	LoadedEnemies();

	template<typename V>
	LoadedEnemies(V&& p_allEnemies) :
		m_allEnemies(std::forward<V>(p_allEnemies)){}


	//getters and setters
	std::vector<Enemy>& getEnemiesRef();

	template<typename T>
	void setEnemies(T&& p_enemiesToSet)
	{
		m_allEnemies = std::forward<T>(p_enemiesToSet);
	}



	//added functionality
	void print() const;
	void populate();

	void putEnemiesPtrsIntoMap(Map& p_myMap);
	void addGuard(Map& p_myMap,LoadedObjects& p_allObjs);
	std::vector<int> createMetaDataForFile() const;
	void saveAllEnamiesIntoFile(const std::string& p_filename)const;
	void loadAllEnemiesFromFile(const std::string& p_filename);


};



















#endif
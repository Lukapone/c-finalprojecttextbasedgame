
#include"SetConsoleWindow.h"
#include"Map.h"
#include"Person.h"
#include"Player.h"
#include"Parser.h"
#include<iostream>
#include"File_handle.h"
#include<stdlib.h>
#include<memory>
#include <iterator>
#include<list>
#include"LoadedLogins.h"
#include"LoginMenu.h"
#include"Startup.h"
#include"MathUtility.h"
#include"Timer.h"
#include <algorithm>
#define _CRTDBG_MAP_ALLOC //for memory leaks
using std::cout;		using std::endl;
using std::cin;			using std::vector;
using std::string;		using std::unique_ptr;
using std::make_unique;	using std::list;

void start();
void timeTestingVector();
void timeTestingList();

int main()
{
#if defined(DEBUG)|defined(_DEBUG)
	//cout << "Getting debug mode" << endl;
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif
	SetWindow(120, 50);

	//start();
	//startNewGame("");
	//startNewGamePopulate();
		
	//timeTestingList();
timeTestingVector();
	cin.ignore();
	return 0;
}


void start()
{
	LoadedLogins allLogins;
	//allLogins.populate();
	
	allLogins.loadAllLoginsFromFile("logins/allLogins.bin");
	//allLogins.print();
	while (printLoginMenu(allLogins) == false){}

	allLogins.saveAllLoginsIntoFile("logins/allLogins.bin");
	//allLogins.print();
}

void timeTestingVector()
{
	cout << "VECTOR" << endl;
	Timer myTimer;
	vector<string> vectorWords{};
	int testingN { 10000 };
	myTimer.Reset();
	vectorWords.reserve(testingN);
	for (int i = 0; i != testingN; i++)
	{
		vectorWords.push_back("abcdefghijklmnoprstuwxyz");
	}
	cout << myTimer.GetMS() << endl;

	myTimer.Reset();
	for (int i = 0; i != vectorWords.size(); i++)
	{
		std::swap(vectorWords[i], vectorWords[vectorWords.size() - 1]);
		//std::swap(vectorWords[i], vectorWords.back());
		//vectorWords[i] = std::move(vectorWords[vectorWords.size() - 1].data());//the move works only with data than is as efective as swap
		vectorWords.pop_back();
		i--;//decrement i after removing the element
	}
	cout << myTimer.GetMS() << endl;
}

void timeTestingList()
{
	cout << "LIST" << endl;
	Timer myTimer;
	list<string> listWords{};
	int testingN{ 10000 };
	myTimer.Reset();

	for (int i = 0; i != testingN; i++)
	{
		listWords.push_back("abcdefghijklmnoprstuwxyz");
	}
	cout << myTimer.GetMS() << endl;

	myTimer.Reset();
	for (int i = 0; i != listWords.size(); i++)
	{
		listWords.pop_front();
	}
	cout << myTimer.GetMS() << endl;

}
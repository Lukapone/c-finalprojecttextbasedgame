//functions which get the input from user
#include"UserInputUtility.h"
#include<iostream>
#include<sstream>//for checking strings into integers
#include<cctype> //defines isspace


using std::cin;		using std::cout;
using std::endl;	using std::string;
using std::istringstream; //for checking convertion from string to int
using std::vector;


//puts > before user input
void expInput()
{
	cout << ">";
}

//function that takes input and returns integer after it was checked
int userInputInt()
{
	int input = 0;
	string inputToCheck = "";
	//this ads > before input
	expInput();
	cin >> inputToCheck;
	if (!(istringstream(inputToCheck) >> input))
	{
		//cout << "convertion from string to int failed please enter integer" << endl;
		input = userInputIntWithText("convertion from string to int failed please enter integer");

	}
	return input;
}

string userInputStr()
{
	string word = "";

	//this ads > before input
	expInput();
	cin >> word;

	return word;
}

//functions that takes input and displays what is expected from user
int userInputIntWithText(const string p_text)
{
	cout << p_text << endl;
	int input = 0;
	string inputToCheck = "";
	//this ads > before input
	expInput();
	cin >> inputToCheck;
	if (!(istringstream(inputToCheck) >> input))
	{
		//cout << "convertion from string to int failed please enter integer" << endl;
		input = userInputIntWithText("convertion from string to int failed please enter integer");

	}
	return input;
}



string userInputStrWithText(const string p_text)
{
	cout << p_text << endl;
	string word = "";

	//this ads > before input
	expInput();
	cin >> word;

	return word;
}

string userInputStrLineWithText(const string p_text)
{
	cout << p_text << endl;
	string word = "";

	//this ads > before input
	expInput();
	getline(cin, word);

	return word;
}
//this takes sentence and returns vector of words
vector<string> getAndSplitInput()
{
	expInput();
	string sentence = "";
	//get line of input from user and store it

	getline(cin, sentence);

	vector<string> words = split(sentence);

	words.shrink_to_fit();//possible save some memory
	return words;

}


//this takes sentence and split it into vector
vector<string> split(const string& s)
{
	vector<string> ret;
	typedef string::size_type stringSize;
	stringSize i = 0;

	//loop throught the characters in sentence and sort them into worlds acording breaks
	while (i != s.size())
	{
		//check if there is space if is jump over it and if we are not at the end of sentence
		while (i != s.size() && isspace(s[i]))
		{
			++i;
		}
		//get the beggining of the word
		stringSize j = i;
		//if there are no spaces and we are not at the end increase j
		while (j != s.size() && !isspace(s[j]))
		{
			++j;
		}

		//if we found some nonwhite spaces put them into vector as one string
		if (i != j)
		{

			ret.push_back(s.substr(i, j - i));
			i = j;
		}
	}//stops at end of the sentence
	//return vector of strings
	return ret;

}
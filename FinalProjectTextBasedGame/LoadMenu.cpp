#include"LoadMenu.h"
#include<iostream>
#include"UserInputUtility.h"
#include"Startup.h"


using std::cin;		using std::cout;
using std::endl;	using std::string;
using std::vector;

enum MenuOptions
{
	LOAD = 1, START_NEW_GAME = 2, QUIT = 3

};

bool printLoadMenu(LoginInfo* p_loggedPlayer)
{
	cout << "Welcome to the load menu of Text Castle Game!" << endl;
	cout << "Press 1 to load game from save" << endl;
	cout << "      2 to start new game" << endl;
	cout << "      3 to quit" << endl;

	return getInputLoadMenu(p_loggedPlayer);
}

//function which gets input from player and checks if there is some save made before loading game otherwise player have to start new game

bool getInputLoadMenu(LoginInfo* p_loggedPlayer)
{
	int userChoice = userInputInt();

	if (userChoice == LOAD)
	{
		//load and than start the game
		if (p_loggedPlayer->getHasSave() == false)
		{
			cout << "No save on this account please start new game." << endl;
		}
		else
		{
			return startFromSave(p_loggedPlayer);
		}
		

	}
	else if (userChoice == START_NEW_GAME)
	{
		//start the game here
		return startNewGame(p_loggedPlayer);

	}
	else if (userChoice == QUIT)
	{
		return true;
	}
	else
	{
		cout << "Invalid input" << endl;
		cin.clear();//clears the buffer
		cin.ignore(80, '\n'); //ignores characters until end of line
	}

	return false;
}
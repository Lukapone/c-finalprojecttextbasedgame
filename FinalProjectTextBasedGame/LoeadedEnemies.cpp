#include"LoadedEnemies.h"
#include<iostream>
#include"BufferManipulation.h"
#include"MathUtility.h"

using std::vector;		using std::cout;
using std::endl;		using std::cin;
using std::string;

LoadedEnemies::LoadedEnemies()
{
}

vector<Enemy>& LoadedEnemies::getEnemiesRef()
{
	return m_allEnemies;
}

void LoadedEnemies::print() const
{
	for (const auto& enemy : m_allEnemies)
	{
		enemy.print();
	}
}

void LoadedEnemies::populate()
{
	
	//Person(STR&& p_name, INT&& p_strenght, INT&& p_agility, INT&& p_currentHealth, INT&& p_maxHealth, INT&& p_level, MapField* p_positionPtr, INT&& p_xPos, INT&& p_yPos)
	Enemy e1("Big Orc", 10, 15, 110, 150, 3, 120, nullptr, 0, 0);
	Enemy e2("Wizard Goblin", 12, 18, 120, 150, 4, 150, nullptr, getRandCoord(), getRandCoord());
	Enemy e3("Dark Elf Paladin", 22, 22, 130, 150, 5, 160, nullptr, getRandCoord(), getRandCoord());
	Enemy e4("Long Elf Warrior", 23, 20, 140, 150, 1, 100, nullptr, getRandCoord(), getRandCoord());
	Enemy e5("Dark Dwarf Paladin", 11, 25, 150, 150, 2, 110, nullptr, getRandCoord(), getRandCoord());
	Enemy e6("Dark Dwarf Warrior", 8, 29, 80, 150, 3, 120, nullptr, getRandCoord(), getRandCoord());
	Enemy e7("Dark Elf Wizard", 9, 32, 85, 150, 4, 170, nullptr, getRandCoord(), getRandCoord());
	Enemy e8("Goblin Shaman", 7, 33, 99, 150, 5, 130, nullptr, getRandCoord(), getRandCoord());
	Enemy e9("Troll Shaman", 6, 36, 120, 150, 6, 140, nullptr, getRandCoord(), getRandCoord());
	Enemy e10("Werewolf", 28, 12, 112, 150, 7, 170, nullptr, getRandCoord(), getRandCoord());
	Enemy e11("Vampire King", 70, 55, 1200, 1200, 10, 300, nullptr,-2, getRandCoord());
	Enemy e12("Dragon", 18, 10, 88, 150, 9, 190, nullptr, getRandCoord(), getRandCoord());
	Enemy e13("Goblin Healer", 17, 15, 85, 150, 6, 160, nullptr, getRandCoord(), getRandCoord());
	Enemy e14("Tauren Rouge", 16, 17, 150, 150, 5, 150, nullptr, getRandCoord(), getRandCoord());
	Enemy e15("Skeleton Archer", 15, 18, 147, 150, 4, 140, nullptr, getRandCoord(), getRandCoord());
	Enemy e16("Ghost", 14, 22, 145, 150, 2, 120, nullptr, getRandCoord(), getRandCoord());
	Enemy e17("Pirate Captain", 13, 25, 130, 150, 2, 120, nullptr, getRandCoord(), getRandCoord());
	Enemy e18("Knight", 12, 26, 77, 150, 1, 120, nullptr, getRandCoord(), getRandCoord());
	Enemy e19("Templar", 11, 31, 65, 150, 1, 120, nullptr, getRandCoord(), getRandCoord());
	Enemy e20("Monk", 10, 30, 148, 150, 1, 120, nullptr, getRandCoord(), getRandCoord());

	m_allEnemies.push_back(e1);
	m_allEnemies.push_back(e2);
	m_allEnemies.push_back(e3);
	m_allEnemies.push_back(e4);
	m_allEnemies.push_back(e5);
	m_allEnemies.push_back(e6);
	m_allEnemies.push_back(e7);
	m_allEnemies.push_back(e8);
	m_allEnemies.push_back(e9);
	m_allEnemies.push_back(e10);
	m_allEnemies.push_back(e11);
	m_allEnemies.push_back(e12);
	m_allEnemies.push_back(e13);
	m_allEnemies.push_back(e14);
	m_allEnemies.push_back(e15);
	m_allEnemies.push_back(e16);
	m_allEnemies.push_back(e17);
	m_allEnemies.push_back(e18);
	m_allEnemies.push_back(e19);
	m_allEnemies.push_back(e20);

}

//function which will add boss into map at random position
//complexity O(c)
void LoadedEnemies::addGuard(Map& p_myMap,LoadedObjects& p_allObjs)
{
	int offsetInVector{ 0 };
	for (auto& enemy : m_allEnemies)
	{
		if (enemy.getXPos() == -2)//only one boos will spawn
		{
			enemy.setXPos(getRandCoord());
			offsetInVector = (enemy.getXPos()*p_myMap.getYlenght()) + enemy.getYPos();//formula to get offset in vector for location
			enemy.pickUpPtr(&p_allObjs.getObjectsRef()[24]);//the number of kings cape
			p_myMap.getFieldsRef()[offsetInVector].addEnemiePtr(&enemy);
			cout << "Boss spawned at position " << enemy.getXPos() << " x coord " << enemy.getYPos() << " y coord." << endl;
			cout << "Kill him and take his cape which will make you the most powerfull warior on the map."<< endl;
		}
	}
}

//function which seperate enemies into map and death enemies and bosses
//complexity O(n)
void LoadedEnemies::putEnemiesPtrsIntoMap(Map& p_myMap)
{
	int offsetInVector{ 0 };
	for (auto& enemy : m_allEnemies)
	{
		if (enemy.getXPos() == -1)
		{
			//this enemies are death i can replenish the healt and put them randomly around the map if i want
		}
		else if (enemy.getXPos() == -2)
		{
			//this is boss so it will be added after you bilded the castle of your own
		}
		else
		{
			offsetInVector = (enemy.getXPos()*p_myMap.getYlenght()) + enemy.getYPos();//formula to get offset in vector for location
			p_myMap.getFieldsRef()[offsetInVector].addEnemiePtr(&enemy);
		}
	}
}

//function which will create the vector of lenghts of the strings from enemie class for each enemie
//complexity O(n)
vector<int> LoadedEnemies::createMetaDataForFile() const
{
	vector<int> allMetadata{};
	allMetadata.push_back(m_allEnemies.size());//push the number of buildings

	for (const auto& enemy : m_allEnemies)
	{
		allMetadata.push_back(enemy.getName().size());
	}

	return allMetadata;

}

//function which will save all enemies into binary file
//complexity O(n) 
void LoadedEnemies::saveAllEnamiesIntoFile(const string& p_filename)const
{
	File_handle fileForSave(p_filename, "wb");
	fileForSave.writeIntegers(this->createMetaDataForFile());
	for (const auto& enemy : m_allEnemies)
	{
		enemy.saveInBinary(fileForSave);
	}
}

//function for loading all enemies from file 
//complexity O(n)
void LoadedEnemies::loadAllEnemiesFromFile(const string& p_filename)
{
	File_handle fileForLoad(p_filename, "rb");
	auto loadedBuffer = fileForLoad.readCharBufferWithUniquePtr();

	int offset{ 0 };
	int numOfEnemies{ getIntfromBuffer(loadedBuffer.get(), offset) };
	vector<int> metadata{ getIntegersfromBuffer(loadedBuffer.get(), offset, numOfEnemies) };

	m_allEnemies.reserve(numOfEnemies);
	Enemy loadedOne;
	int counter{ 0 };
	for (int i = 0; i != numOfEnemies; i++)
	{
		loadedOne.loadFromBuffer(metadata[i], offset, loadedBuffer.get());
		m_allEnemies.push_back(std::move(loadedOne));
	}

}



#include"Parser.h"
#include"PrintUtility.h"
#include"UserInputUtility.h"
#include"StringUtility.h"
#include<iostream>
#include"Player.h"
#include"Startup.h"

using std::vector;		using std::string;
using std::cout;		using std::cin;
using std::endl;



typedef vector<string> stringContainer;

//function which takes the user input and parses it to usefull comands for game
//the worst complexity insode is O(n2)
bool parse(const stringContainer& sentence, Map& p_myMap, Player& p_player, LoadedBuildings& p_allBuildings, LoadedObjects& p_allObjs, LoadedEnemies& p_allEnemies, LoginInfo* p_loggedPlayer)
{
	if (foundInSentence(sentence, "go") == true)
	{
		checkDirection(sentence, p_myMap, p_player);//for north, west, south, east
	}
	else if (foundInSentence(sentence, "fight") == true)
	{
		p_player.fightIfExists(p_myMap);
	}
	else if (foundInSentence(sentence, "use") == true)
	{
		if (p_player.useAllKeyes() == true)
		{
			//let him create castle
			cout << "You can create your own building." << endl;
			p_allBuildings.createBuilding(p_myMap);
			p_allEnemies.addGuard(p_myMap,p_allObjs);
		}
		else
		{
			cout << "You dont have enough keyes to make your own castle." << endl;
		}
	}
	else if (foundInSentence(sentence, "rest") == true)
	{
		cout << "You rested whole day and replenished your health." << endl;
		p_player.healTotaly();//whe you rest you wiil heal yourself
	}
	else if (foundInSentence(sentence, "equip") == true)
	{
		p_player.equipIfExists(sentence);
	}
	else if (foundInSentence(sentence, "unequip") == true)
	{
		p_player.unequipIfExists(sentence);
	}
	else if (foundInSentence(sentence, "visit") == true)
	{
		p_player.visitBuilding();
	}
	else if (foundInSentence(sentence, "pick") == true)
	{
		if (p_player.pickUpIfExists(sentence))
		{
			system("cls");//commenting this will disable clearing the screen
			p_player.printMapFieldYouStandOn();//this will disable print after pickups if commented out
		}
			
		//p_player.pickUpIfExists(sentence);
	}
	else if (foundInSentence(sentence, "drop") == true)
	{
		if (p_player.dropIfExists(sentence))
		{
			system("cls");//commenting this will disable clearing the screen
			p_player.printMapFieldYouStandOn();//this will disable print after pickups if commented out
		}
			
		//p_player.dropIfExists(sentence);
	}
	else if (foundInSentence(sentence, "show") == true)
	{
		p_player.print();
		p_player.printInventory();
	}
	else if (foundInSentence(sentence, "look") == true)
	{
		p_player.printMapFieldYouStandOn();
	}
	else if (foundInSentence(sentence, "exit") == true)
	{
		p_player.setXandYPosFromPtr();//set the x and y position for save
		p_allBuildings.saveAllBuildingsIntoFile(FOLDER_FOR_SAVE + p_loggedPlayer->getName() + FILE_NAME_BUILDINGS);
		p_allObjs.saveAllObjectsIntoFile(FOLDER_FOR_SAVE + p_loggedPlayer->getName() + FILE_NAME_OBJECTS);
		p_allEnemies.saveAllEnamiesIntoFile(FOLDER_FOR_SAVE + p_loggedPlayer->getName() + FILE_NAME_ENEMIES);
		p_player.saveToFile(FOLDER_FOR_SAVE + p_loggedPlayer->getName() + FILE_NAME_PLAYER);
		p_loggedPlayer->setHasSave(true);//now you can load
		return true;
	}
	else if (foundInSentence(sentence, "save") == true)
	{
		p_player.setXandYPosFromPtr();//set the x and y position for save
		p_allBuildings.saveAllBuildingsIntoFile(FOLDER_FOR_SAVE + p_loggedPlayer->getName() + FILE_NAME_BUILDINGS);
		p_allObjs.saveAllObjectsIntoFile(FOLDER_FOR_SAVE + p_loggedPlayer->getName() + FILE_NAME_OBJECTS);
		p_allEnemies.saveAllEnamiesIntoFile(FOLDER_FOR_SAVE + p_loggedPlayer->getName() + FILE_NAME_ENEMIES);
		p_player.saveToFile(FOLDER_FOR_SAVE + p_loggedPlayer->getName() + FILE_NAME_PLAYER);
		p_loggedPlayer->setHasSave(true);//now you can load
		cout << "You saved the game." << endl;
	}
	else if (foundInSentence(sentence, "help") == true)
	{
		cout << R"(Available commands are : go, fight, use keys, rest, equip, unequip, 
					visit, pick, drop, show inventory, look around, exit game, save game)" << endl;
	}
	else
	{
		//no words found that ment something to parser return no option
		cout << "No keywords enterred " << endl;
	}

	return false;
}

//function which searches the vector for particular word and returns true if it finds it
//complexity is O(n) longer the sentence longer it takes
bool foundInSentence(const stringContainer& sentence, const string& toSearchFor)//get meaning to word for example open== asociated with door in current room
{
	for (const auto& word : sentence)
	{
		if (toLowerCase(word) == toSearchFor)
		{
			return true;
		}
	}
	return false;//didnt found the word
}

//function which moves player around the map
//the worst complexity is O(n)
void checkDirection(const stringContainer& sentence, Map& p_myMap, Player& p_player)
{
	if (foundInSentence(sentence, "north") || foundInSentence(sentence, "n"))
	{
		if (p_player.goNorth(p_myMap) == true)
		{
			cout << "You are going North." << " Your new position is: " << endl;
			p_player.printMapFieldYouStandOn();//this will print everithing you can see items, buildings, enemies
		}
		
	}
	else if (foundInSentence(sentence, "south") || foundInSentence(sentence, "s"))
	{
		if (p_player.goSouth(p_myMap) == true)
		{
			cout << "You are going South." << " Your new position is: " << endl;
			p_player.printMapFieldYouStandOn();
		}
	}
	else if (foundInSentence(sentence, "east") || foundInSentence(sentence, "e"))
	{
		if (p_player.goEast(p_myMap) == true)
		{
			cout << "You are going East." << " Your new position is: " << endl;
			p_player.printMapFieldYouStandOn();
		}
	}
	else if (foundInSentence(sentence, "west") || foundInSentence(sentence, "w"))
	{
		if (p_player.goWest(p_myMap) == true)
		{
			cout << "You are going West." << " Your new position is: " << endl;
			p_player.printMapFieldYouStandOn();
		}
	}
	else
	{
		//sorry no option
		cout << "You canot go there" << endl;
	}
}

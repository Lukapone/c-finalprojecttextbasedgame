
#include"MathUtility.h"
#include"Player.h"
#include"Enemy.h"
#include"Enums.h"
#include<iostream>
#include"BufferManipulation.h"
#include"UserInputUtility.h"
using std::cout;		using std::endl;

Player::Player() :Person(), m_currentExperience(DEFAULT_START_EXP){}

bool Player::useAllKeyes()
{
	return m_inventory.useAllKeyes();
}

//this checks the field we are standing on and start fight with first enemy
//the worst complexity inside is O(n)
bool Player::fightIfExists(Map& p_myMap)
{
	Enemy* ptrToOneYouFight = m_positionPtr->getEnemyToFightAndRemoveIt();
	if (ptrToOneYouFight == nullptr)
	{
		//there was nobody to fight
		return false;
	}
	else
	{
		this->fightSimulation(ptrToOneYouFight,p_myMap);
	}
	return true;
}

//function which will simulate fight with enemie
//complexity is order O(n)
void Player::fightSimulation(Enemy* p_eToFight,Map& p_myMap)
{
	while (true)
	{
		if (!this->swingEnemy(p_eToFight))
		{
			cout << "You killed " << p_eToFight->getName() << endl;
			//add experience
			this->addExperience(p_eToFight->getExperiencveReward());
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			if (p_eToFight->getInventory().getAllObjectsPtrs().empty() == false)//if is not empty drop
			{
				m_positionPtr->addDroppedObjectPtr(p_eToFight->getInventory().getRandomObjPtrAndRemove(p_eToFight));//drop object on the floor
				cout << "Enemy dropped object " << endl;
			}
			break;
		}

		if (!this->getHitByEnemy(p_eToFight))
		{
			cout << "You are death respawning at random position setting curr exp to 50% and dropping random item." << endl;
			//set experience to 0
			if (m_inventory.getAllObjectsPtrs().empty() == false)//if is not empty drop
			{
				m_positionPtr->addDroppedObjectPtr(m_inventory.getRandomObjPtrAndRemove(this));//drop object on the floor
			}	
			m_currentExperience = m_currentExperience-(m_currentExperience/2);
			m_positionPtr->addWoundetEnemiePtr(p_eToFight);//add the woundet enemy back
			//cout << "Added enemy back" << endl;
			//respawn set health to be full again
			this->healTotaly();
			m_positionPtr = &p_myMap.getFieldsRef()[getRandCoord()];//get random number instead of 0
			break;
		}
	}
	cout << "Fight simulation ended" << endl;
}

//function which will calculate damage in one swing you have done to enemie
//complexity O(c)
bool Player::swingEnemy(Enemy* p_eToSwing)
{
	int bonusStr{ 0 };
	int bonusAgi{ 0 };
	int randNum = Random(0, 10);
	if (this->addBonusStrength())
	{
		bonusStr = SWING_BONUS_STR + randNum;
	}
	else
	{
		bonusAgi = SWING_BONUS_AGI + randNum;
	}
	int damageDealth{ m_strenght+bonusStr };//take some percent from hit depending on enemies str
	randNum = Random(0, 100);
	if (m_agility+bonusAgi >= randNum)//got critical strike
	{
		damageDealth = damageDealth * 2;
		cout << "CRITICAL STRIKE" << endl;
	}
	cout << "You hit the enemy for " << damageDealth << " damage." << endl;
	return p_eToSwing->takeDamage(damageDealth);
}

//function which will buff players strength or agility depending on players choice
//complaxity O(c)
bool Player::addBonusStrength() const
{
	int num{ userInputIntWithText("Please enter 1 to buff strength or any number to buff agility") };
	if (num == 1)
	{
		return true;
	}
	return false;
}

//function which will calculate damage in one swing you have taken fom enemie
//complexity O(c)
bool Player::getHitByEnemy(Enemy* p_eToByHitBy)
{
	int damageToTake{ p_eToByHitBy->getStrenght() };
	int randNum = Random(1, 100);
	if (p_eToByHitBy->getAgility() >= randNum)//got critical strike
	{
		damageToTake = damageToTake * 2;
		cout << "CRITICAL STRIKE" << endl;
	}
	cout << "You got hit by the enemy for " << damageToTake << " damage." << endl;
	return this->takeDamage(damageToTake);
}

//function which adds experience and check if you leveled up
//complexity O(c)
void Player::addExperience(int p_expToAdd)
{
	m_currentExperience = m_currentExperience + p_expToAdd;
	this->checkForLevelUp();
}

//function which will check if your level and experience are enough to level up
//complexity O(c)
void Player::checkForLevelUp()
{
		if (m_level == 10)//if you are max level do nothing
		{
		}
		else if (m_level == 1)
		{
			if (m_currentExperience >= LEVEL1)
				this->addLevel();
		}
		else if (m_level == 2)
		{
			if (m_currentExperience >= LEVEL2)
				this->addLevel();
		}
		else if (m_level == 3)
		{
			if (m_currentExperience >= LEVEL3)
				this->addLevel();
		}
		else if (m_level == 4)
		{
			if (m_currentExperience >= LEVEL4)
				this->addLevel();
		}
		else if (m_level == 5)
		{
			if (m_currentExperience >= LEVEL5)
				this->addLevel();
		}
		else if (m_level == 6)
		{
			if (m_currentExperience >= LEVEL6)
				this->addLevel();
		}
		else if (m_level == 7)
		{
			if (m_currentExperience >= LEVEL7)
				this->addLevel();
		}
		else if (m_level == 8)
		{
			if (m_currentExperience >= LEVEL8)
				this->addLevel();
		}
		else
		{
			if (m_currentExperience >= LEVEL9)
				this->addLevel();
		}
}

void Player::do_print()const
{
	cout << "Current experience: " << m_currentExperience << endl;
}

void Player::do_saveBinary(File_handle& p_fileToSave)const
{
	p_fileToSave.writeInt(m_currentExperience);
}

//function which adds level and health bonus to max health
//complexity O(c)
void Player::addLevel()
{
	cout << "You leveled up." << endl;
	m_level++;
	m_currentExperience = 0;
	m_maxHealth = m_maxHealth + HEALTHBONUS;
}

void Player::do_loadFromBuffer(char* p_buffer, int& p_offset)
{
	m_currentExperience = getIntfromBuffer(p_buffer, p_offset);
}

void Player::do_loadFromFile(File_handle& p_fileToLoad)
{
	m_currentExperience = p_fileToLoad.readIntFromFileAtPointer();
}
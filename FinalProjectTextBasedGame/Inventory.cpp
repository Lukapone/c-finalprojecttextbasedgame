
#include"Inventory.h"
#include"Person.h"
#include"StringUtility.h"
#include<iostream>
#include<utility>
#include"MathUtility.h"

using std::vector;		using std::cout;
using std::string;		using std::cin;
using std::endl;

Inventory::Inventory() :m_numOfEquippedObjs(DEFAULT_NUM_OF_EQUIPPED_ITEMS)
{
	m_objcetsPtrs.reserve(DEFAULT_INIT_SIZE);//this will reserve the size of default inventory
}

Inventory::Inventory(const int p_initSize) : m_numOfEquippedObjs(DEFAULT_NUM_OF_EQUIPPED_ITEMS)
{
	m_objcetsPtrs.reserve(p_initSize);
}
//function which adds a object ptr to inventory
//constant order O(c)
void Inventory::addToInventoryPtr(Object* p_toAdd)
{
	if (p_toAdd->getObjYPos() == IS_EQUIPPED)
	{
		if (m_numOfEquippedObjs == MAX_EQUIPPED)
		{
			cout << "You are going over the limit of equipped items" << endl;
		}
		m_numOfEquippedObjs = m_numOfEquippedObjs + 1;//i know this wont go over the limit because i am loading from file
	}
	m_objcetsPtrs.push_back(p_toAdd);
}

//function which returns vector of objects pointers 
//O(c)
vector<Object*>& Inventory::getAllObjectsPtrs()
{
	return m_objcetsPtrs;
}

//function which drops the item from inventory if it is not equipped
//order is O(n2) it doubles for the size of the vector of objects and vector of strings
Object* Inventory::getObjPtrByNameAndRemove(const std::vector<std::string>& p_sentence)
{
	Object* toReturn{ nullptr };
	for (const auto& word : p_sentence)
	{
		for (int i = 0; i != m_objcetsPtrs.size(); i++)
		{
			if (toLowerCase(word) == toLowerCase(m_objcetsPtrs[i]->getObjName()))
			{
				if (m_objcetsPtrs[i]->getObjYPos() == IS_EQUIPPED)
				{
					cout << "You canot drop equiped item." << endl;
				}
				else
				{
					cout << "You dropped: " << m_objcetsPtrs[i]->getObjName() << endl;
					toReturn = m_objcetsPtrs[i];
					std::swap(m_objcetsPtrs[i], m_objcetsPtrs[m_objcetsPtrs.size() - 1]);//swap elements for fast removal from vector just pop_back
					m_objcetsPtrs.pop_back();							//i used vector and fast removel because it is as fast as linked list but less memory consumption
					return toReturn;
				}
			}
		}
	}
	cout << "You didnt drop anything." << endl;
	return toReturn;
}

//function which will return random object from inventory unequip it if is equipped
//complexity O(c)
Object* Inventory::getRandomObjPtrAndRemove(Person* p_player)
{
	Object* toReturn{ nullptr };
	int randObj = Random(0, m_objcetsPtrs.size() - 1);
	if (m_objcetsPtrs[randObj]->getObjYPos() == IS_EQUIPPED)
	{
		m_numOfEquippedObjs = m_numOfEquippedObjs - 1;
		m_objcetsPtrs[randObj]->setObjYPos(0);
		p_player->removeObjectBonuses(m_objcetsPtrs[randObj]);
		toReturn = m_objcetsPtrs[randObj];
		std::swap(m_objcetsPtrs[randObj], m_objcetsPtrs[m_objcetsPtrs.size() - 1]);//swap elements for fast removal from vector just pop_back
		m_objcetsPtrs.pop_back();
	}
	else
	{
		toReturn = m_objcetsPtrs[randObj];
		std::swap(m_objcetsPtrs[randObj], m_objcetsPtrs[m_objcetsPtrs.size() - 1]);//swap elements for fast removal from vector just pop_back
		m_objcetsPtrs.pop_back();
	}
				
	return toReturn;
}

//function which will check the sentence for object and equip it if is not equipped already
//order is O(n2)
bool Inventory::setToEquipped(const vector<string>& p_sentence,Person* p_player)
{
	if (m_numOfEquippedObjs == MAX_EQUIPPED)
	{
		cout << "You canot equip more items unequip some in order to change them." << endl;
		return false;
	}
	for (const auto& object : m_objcetsPtrs)
	{
		for (const auto& word : p_sentence)
		{
			if (toLowerCase(word) == toLowerCase(object->getObjName()))
			{
				if (object->getObjYPos() == IS_EQUIPPED)
				{	
					cout << "The object is equipped." << endl;
					return false;
				}
				else
				{
					m_numOfEquippedObjs = m_numOfEquippedObjs + 1;
					object->setObjYPos(IS_EQUIPPED);
					p_player->getObjectBonuses(object);
					cout << "You equipped: " << object->getObjName() << endl;
					return true;
				}	
			}
		}		
	}
	cout << "No object by that name in Inventory." << endl;
	return false;
}

//function which will check the sentence for object and uequip it if is not uequipped already
//order is O(n2)
bool Inventory::removeFromEquipped(const vector<string>& p_sentence, Person* p_player)
{
	for (const auto& object : m_objcetsPtrs)
	{
		for (const auto& word : p_sentence)
		{
			if (toLowerCase(word) == toLowerCase(object->getObjName()))
			{
				if (object->getObjYPos() == IS_EQUIPPED)
				{
					m_numOfEquippedObjs = m_numOfEquippedObjs - 1;
					object->setObjYPos(0);
					p_player->removeObjectBonuses(object);
					cout << "You unequipped: " << object->getObjName() << endl;
					return true;
				}
				else
				{
					cout << "The object is not equipped." << endl;
					return false;
				}
			}
		}
	}
	cout << "No object by that name in Inventory." << endl;
	return false;
}

void Inventory::print() const
{
	for (const auto& object : m_objcetsPtrs)
	{
		object->print();
	}
}

//function which wil check the inventory for 3keys and if they are in remove them and returns true
//complexity is O(n) the more object is in inventory longer it takes
bool Inventory::useAllKeyes()
{
	int counter{ 0 };
	for (const auto& object : m_objcetsPtrs)
	{
		if (object->getObjName() == "Key")
		{
			counter++;
			if (counter == 3)
			{
				break;
			}
		}
	}
	if (counter == 3)
	{
		for (int i = 0; i != m_objcetsPtrs.size(); i++)
		{
			if (m_objcetsPtrs[i]->getObjName() == "Key")
			{		
				m_objcetsPtrs[i]->setObjXPos(USED_KEY);
				std::swap(m_objcetsPtrs[i], m_objcetsPtrs[m_objcetsPtrs.size()-1]);//i used vector and fast removel because it is as fast as linked list but less memory consumption
				m_objcetsPtrs.pop_back(); 
				i--;//decrement i after removing the element
				counter--;
				if (counter == 0)
				{
					break;
				}
			}
		}
		return true;
	}
	return false;
}
#include"CharacterCustomization.h"
#include"UserInputUtility.h"
#include<iostream>

using std::cout;		using std::endl;

static const int POINTS_TO_DISTRIBUTE = 20;

//not used in game but this would loop and add each point separately until no points left for customizing character
//complexity O(n)
void customizeCharacterInLoop(Player& p_toCustomize)
{
	p_toCustomize.setName(userInputStrWithText("Please enter yours charaters name:"));
	for (int i = 0; i != POINTS_TO_DISTRIBUTE; i++)
	{
		int userChoice = userInputIntWithText("Enter 1 to add point to strength 2 to add point to agility and 3 to add 20 points to health ");
		if (userChoice == 1)
		{
			p_toCustomize.addStrenght(1);
		}
		else if (userChoice==2)
		{
			p_toCustomize.addAgility(1);
		}
		else
		{
			p_toCustomize.addMaxHealth(20);
		}

	}

	p_toCustomize.print();

}

//function which lets you to add points to strenght agility and max health
bool customizeCharacter(Player& p_toCustomize)
{
	cout << "You have " <<POINTS_TO_DISTRIBUTE<< " points to distribute between strength agility and health.(non distributed points are lost)" << endl;
	int points{ POINTS_TO_DISTRIBUTE };
	int strToAdd{ userInputIntWithText("Enter how much strength you want") };
	if (strToAdd <= points)
	{
		p_toCustomize.addStrenght(strToAdd);
		points = points - strToAdd;
	}
	else
	{
		cout << "Not enough points please start again." << endl;
		return false;
	}
	cout << "You have "<< points << " points left to distribute." << endl;
	int agiToAdd{ userInputIntWithText("Enter how much agility you want") };
	if (agiToAdd <= points)
	{
		p_toCustomize.addAgility(agiToAdd);
		points = points - agiToAdd;
	}
	else
	{
		p_toCustomize.addStrenght(-strToAdd);
		cout << "Not enough points please start again." << endl;
		return false;
	}
	cout << "You have " << points << " points left to distribute." << endl;
	int healthToAdd{ userInputIntWithText("Enter how much health you want(each point is 20 health)") };
	if (healthToAdd <= points)
	{
		p_toCustomize.addMaxHealth(healthToAdd*20);
		p_toCustomize.healTotaly();//set the current health to be full
	}
	else
	{
		p_toCustomize.addStrenght(-strToAdd);
		p_toCustomize.addAgility(-agiToAdd);
		cout << "Not enough points please start again." << endl;
		return false;
	}
	//p_toCustomize.print();
	return true;
}


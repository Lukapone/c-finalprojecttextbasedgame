
#include"Startup.h"
#include"PrintUtility.h"
#include"UserInputUtility.h"
#include<iostream>
#include"LoadedBuildings.h"
#include"LoadedObjects.h"
#include"LoadedEnemies.h"
#include"Player.h"
#include"Parser.h"
#include"CharacterCustomization.h"
#include"LoginInfo.h"
#include"MathUtility.h"
#include"Timer.h"

using std::cout;		using std::endl;
using std::string;		using std::cin;
using std::vector;

//start game from save and load all the needet info based on logged player
bool startFromSave(LoginInfo* p_loggedPlayer)
{
	Player player;
	player.loadFromFile(FOLDER_FOR_SAVE + p_loggedPlayer->getName() + FILE_NAME_PLAYER);

	LoadedBuildings allBuildings{};
	allBuildings.loadAllBuildingsFromFile(FOLDER_FOR_SAVE + p_loggedPlayer->getName() + FILE_NAME_BUILDINGS);

	LoadedObjects allObjects{};
	allObjects.loadAllObjectsFromFile(FOLDER_FOR_SAVE + p_loggedPlayer->getName() + FILE_NAME_OBJECTS);

	LoadedEnemies allEnemies{};
	allEnemies.loadAllEnemiesFromFile(FOLDER_FOR_SAVE + p_loggedPlayer->getName() + FILE_NAME_ENEMIES);

	Map myMap{};
	myMap.populateMap();

	allBuildings.putBuildingPtrsIntoMap(myMap);
	allObjects.putObjectsPtrsIntoMap(myMap,player,allBuildings);
	allEnemies.putEnemiesPtrsIntoMap(myMap);

	//have to set position or it crashes
	player.setPositionPtrFromMap(myMap);
	player.print();
	player.printMapFieldYouStandOn();

	//game loop
	bool done = false;
	while (!done)
	{
		vector<string> sentence;
		while (sentence.size() == 0)//this will get rid of 'no keywords enterred when he does not type anything
		{
			sentence = getAndSplitInput();//get input to parse
		}
		Timer myTimer;
		lint myMs = myTimer.GetMS();
		myTimer.Reset();
		//parse any input and give you answer
		done = parse(sentence, myMap, player, allBuildings, allObjects, allEnemies, p_loggedPlayer);
		cout <<"Parse function took :"<< myTimer.GetMS()<<" ms" << endl;
	}
	return true;
}

//start new game for logged player
bool startNewGame(LoginInfo* p_loggedPlayer)
{

	//print intro here                     
	intro();
	Player player("noName", 10, 10, 100, 100, 1, 0, nullptr, 0, 0);// getRandCoord(),getRandCoord()
	//player customization next
	player.setName(userInputStrWithText("Please enter yours charaters name:"));
	while (customizeCharacter(player) == false){}

	LoadedBuildings allBuildings{};
	allBuildings.loadAllBuildingsFromFile(FILE_NAME_BUILDINGS);

	LoadedObjects allObjects{};
	allObjects.loadAllObjectsFromFile(FILE_NAME_OBJECTS);

	LoadedEnemies allEnemies{};
	allEnemies.loadAllEnemiesFromFile(FILE_NAME_ENEMIES);

	Map myMap{};
	myMap.populateMap();

	allBuildings.putBuildingPtrsIntoMap(myMap);
	allObjects.putObjectsPtrsIntoMap(myMap,player,allBuildings);
	allEnemies.putEnemiesPtrsIntoMap(myMap);

	//have to set position or it crashes
	player.setPositionPtrFromMap(myMap);
	player.print();
	player.printMapFieldYouStandOn();

	//game loop
	bool done = false;
	while (!done)
	{
		vector<string> sentence;
		while (sentence.size() == 0)//this will get rid of 'no keywords enterred when he does not type anything
		{
			sentence = getAndSplitInput();//get input to parse
		}
			Timer myTimer;
		lint myMs = myTimer.GetMS();
		myTimer.Reset();
		//parse any input and give you answer
		done = parse(sentence, myMap, player, allBuildings, allObjects, allEnemies, p_loggedPlayer);
		cout << "Parse function took :" << myTimer.GetMS() << " ms" << endl;
	}
	return true;
}

//start for new game if we dont have file and need to populate them not used in game
bool startNewGamePopulate()
{
	//print intro here
	intro();

	LoginInfo p_loggedPlayer;
	Player player("noName", 1000, 10, 1100, 1100, 1, 0, nullptr, 0, 0);
	//player customization next
	//player.setName(userInputStrWithText("Please enter yours charaters name:"));
	//while (customizeCharacter(player) == false){}

	LoadedBuildings allBuildings{};
	allBuildings.populate();
	allBuildings.saveAllBuildingsIntoFile(FILE_NAME_BUILDINGS);
	//allBuildings.print();

	LoadedObjects allObjects{};
	allObjects.populate();
	allObjects.saveAllObjectsIntoFile(FILE_NAME_OBJECTS);
	//allObjects.print();

	LoadedEnemies allEnemies{};
	allEnemies.populate();
	allEnemies.saveAllEnamiesIntoFile(FILE_NAME_ENEMIES);

	Map myMap{};
	myMap.populateMap();

	allBuildings.putBuildingPtrsIntoMap(myMap);
	allObjects.putObjectsPtrsIntoMap(myMap,player,allBuildings);
	//allEnemies.putEnemiesPtrsIntoMap(myMap);

	//myMap.print();
	player.setPositionPtrFromMap(myMap);//this takes pointer to the location from map 
	player.print();
	player.printMapFieldYouStandOn();

	//game loop
	bool done = false;
	while (!done)
	{
		vector<string> sentence;
		while (sentence.size() == 0)//this will get rid of 'no keywords enterred when he does not type anything
		{
			sentence = getAndSplitInput();//get input to parse
		}
		//parse any input and give you answer
		done = parse(sentence, myMap, player, allBuildings, allObjects, allEnemies, &p_loggedPlayer);
	}
	player.print();
	cout << "Exiting game" << endl;
	return true;
}

void intro()
{
	//you can disable typing with cancelling the delay
	println(R"(		Welcome adventurer into land of kings, castles, monsters, items and text of course.
	
		Text will guide you thought the harsh land of Walk n Type continent so you better read
		what is written on your screen. Around the map is many castles with keys inside. 
		Your mission is to collect at least three keys to be able to build your own building so good luck.)"); 
}
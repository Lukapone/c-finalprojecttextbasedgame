#ifndef MAPFIELD_H
#define MAPFIELD_H

#include"Building.h"
#include"Object.h"
#include<utility>
#include<vector>

class Enemy;
//class which models one map field in game 
class MapField
{
private:
	int m_xCoord;
	int m_yCoord;
	//i used pointers to buildings, objects, enemies for fast and easy manipulation with vector and low memory consumption with swap for fast removal 
	std::vector<Building*> m_buildingsOnFieldPtrs; 
	std::vector<Object*> m_objectsPtrs;
	std::vector<Enemy*> m_enemiesPtrs;


	//default constant values if you change here all uses are changed
	static const int DEFAULT_X_COORD = 0;
	static const int DEFAULT_Y_COORD = 0;

public:
	MapField();

	template <typename INT>
	MapField(INT&& p_xCoord, INT&& p_yCoord) :
		m_xCoord(std::forward<INT>(p_xCoord)),
		m_yCoord(std::forward<INT>(p_yCoord))
	{
	}

	//getters and setters
	int getX_coord() const;
	int getY_coord() const;

	std::vector<Building*>& getBuildingsPtrs();
	std::vector<Object*>& getObjectsPtrs();
	std::vector<Enemy*>& getEnemiesPtrs();

	template<typename T>
	void setX_coord(T&& p_xCoord)
	{
		m_xCoord = std::forward<T>(p_xCoord);
	
	}

	template<typename T>
	void setY_coord(T&& p_yCoord)
	{
		m_yCoord = std::forward<T>(p_yCoord);
	}

	//added functionality
	void print() const;
	void printAllBuildings()const;
	void printAllObjects()const;
	void printAllEnemies()const;


	void addObjectPtr(Object* p_objToAdd);
	void addDroppedObjectPtr(Object* p_objToAdd);
	void addBuildingPtr(Building* p_buildingToAdd);
	void addEnemiePtr(Enemy* p_enemyToAdd);
	void addWoundetEnemiePtr(Enemy* p_enemyToAdd);

	Object* getObjPtrByNameAndRemove(const std::vector<std::string>& sentence);
	Enemy* getEnemyToFightAndRemoveIt();
	bool hasBuilding() const;
	bool hasEnemies() const;


};



#endif
#ifndef LOGIN_INFO_H
#define LOGIN_INFO_H
#include<string>
#include<utility>
#include"File_handle.h"

static const char DEFAULD_UNAME[] { "noUserName"};
static const char DEFAULD_PASSWORD[] { "noPassword"};
//class which takes care of players login informations

class LoginInfo
{
private:
	std::string m_userName;
	std::string m_password;
	bool m_hasSave;
public:

	LoginInfo();

	template<class STR1,class STR2>
	LoginInfo(STR1&& p_userName, STR2&& p_password) :
		m_userName(std::forward<STR1>(p_userName)),
		m_password(std::forward<STR2>(p_password)),
		m_hasSave(false)
	{}

	std::string getName()const;
	std::string getPassword()const;
	bool getHasSave()const;

	template<class T>
	void setName(T&& p_name)
	{
		m_userName = std::forward<T>(p_name);
	}

	template<class T>
	void setPassword(T&& p_password)
	{
		m_password = std::forward<T>(p_password);
	}

	template<class T>
	void setHasSave(T&& p_hasSave)
	{
		m_hasSave = std::forward<T>(p_hasSave);
	}

	//added functions
	void print() const;

	void saveLoginInBinary(File_handle& p_fileToSave)const;

	void loadLoginFromBuffer(int p_nameSize,int p_passwordSize, int& p_offset, char* p_buffer);

};



#endif
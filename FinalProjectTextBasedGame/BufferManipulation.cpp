#include "BufferManipulation.h"


#include<iostream>
using std::cout;		using std::endl;
using std::string;		
using std::vector;

//function which takes pointer to the buffer and reference to the offset at which we want to extract integer and changes the offset
//complexity is constant order O(c)
int getIntfromBuffer(char* p_bufferPtr, int& p_offset)
{
	int readedInt{ 0 };
	if (memcpy_s(&readedInt, sizeof(int), p_bufferPtr + p_offset, sizeof(int)) == 0)
	{
		//cout << "coppied " << readedInt << endl;
	}
	else
	{
		cout << "error returning 0" << endl;
	}
	p_offset =p_offset+ sizeof(int);
	//cout << "Offset after call;"<<p_offset << endl;
	return readedInt;
}

//functions which takes pointer to the buffer and reference to the offset at which we want to extract character and changes the offset
//complexity is constant order O(c)
char getCharfromBuffer(char* p_bufferPtr, int& p_offset)
{
	char readedChar{ 0 };
	if (memcpy_s(&readedChar, sizeof(char), p_bufferPtr + p_offset, sizeof(char)) == 0)
	{
		//cout << "coppied " << readedInt << endl;
	}
	else
	{
		cout << "error returning 0" << endl;
	}
	p_offset = p_offset + sizeof(char);
	//cout << "Offset after call;"<<p_offset << endl;
	return readedChar;
}

//function which takes pointer to the buffer and reference to the offset at which we want to extract integers and number of how many and changes the offset
//complaxity is O(n) the more you want to extract longer it takes
vector<int> getIntegersfromBuffer(char* p_bufferPtr, int& p_offset, int p_numberOfInts)
{
	vector<int> allInts{};
	
	for (int i = 0; i != p_numberOfInts; i++)
	{
		allInts.push_back(getIntfromBuffer(p_bufferPtr, p_offset));
	}

	return allInts;
}

//function which takes pointer to the buffer and reference to the offset at which we want to extract string and how long is the string and changes the offset
//complaxity is O(n) longer the string longer it takes
string getStringFromBuffer(char* p_bufferPtr, int &p_offset, int p_strLenght)
{
	string toRet{};
	for (int i = 0; i != p_strLenght; i++)
	{
		toRet.push_back(p_bufferPtr[p_offset + i]);
	}
	p_offset = p_offset + p_strLenght;
	return toRet;
}